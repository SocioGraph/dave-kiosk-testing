# Logs and Reports Directory

All reports and logs for each run will be stored here. The auto-generated directory name will be displayed at the end of each run.