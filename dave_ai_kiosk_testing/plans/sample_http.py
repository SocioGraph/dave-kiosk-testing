import time
from locust import HttpUser, task, between
from faker import Faker
import random

# Fake data generation for testing
fake = Faker()

capacities = [20, 30, 40, 50, 60, 70]
establishments = list(range(1991,2021))
ratings = [2, 2.5, 3, 3.5, 4, 4.5, 5]
phone_numbers = [993456789, 889956789, 783456789, 993456129, 987456789, 895856789]

class QuickStarUser(HttpUser):
    wait_time = between(1, 5)
    host = "http://localhost:5000"

    @task
    def home(self):
        self.client.get("/")
    
    @task
    def overview(self):
        self.client.get("/go_to/overview")

    @task
    def add_object_to_model(self):
        new_object = {"service_center_name": fake.name() + " Service Center",
                        "established": random.choice(establishments),
                        "max_capacity": random.choice(capacities),
                        "manager_name": fake.name(),
                        "ratings": random.choice(ratings),
                        "phone_number": random.choice(phone_numbers)
                        }
        self.client.post("/object/car_service_centers", json=new_object, catch_response=True)

    def on_start(self):
        auth = {"enterprise_id":"maruti_subscription",
                "user_id":"ananth+maruti_subscription@i2ce.in",
                "password":"MS1LS^bscribe"}
        self.client.post("/login", json=auth, catch_response=True)
