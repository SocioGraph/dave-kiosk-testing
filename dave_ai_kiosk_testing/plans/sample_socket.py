"""
In this file, a sample socket server will be tested. 
The user must run a sample socket server with an event "my_event". For example, use the single server.

"""
from os import PRIO_PGRP
from locust import task, between, User
import socketio
import time
import random
import csv
from dave_ai_kiosk_testing.settings import REPORTS_DIR, REPORT_PATH

# CSV Headers
HEADERS = ['time_sent','data_sent','time_received', 'data_gotback']

# CSV Filename
SOCKET_REPORT_DIR = ""
FILE_PATH = REPORT_PATH.joinpath("requests_data_exchange.csv")

# URL
URL = "ws://127.0.0.1:6000/"

class UserBehavior(User):

    wait_time = between(1, 5)

    statements = statements = ['Statement 1', 'Statement 2', 'Statement 3', 'Statement 4']

    def server_returns(self, *args, **kwargs):
        response_data = args[0]
        writing_data = {
            "time_sent": response_data["start_at"],
            "data_sent": str(response_data["message"]) + " " + str(response_data["start_at"]),
            "time_received": time.time(),
            "data_gotback": str(self.body["message"]) + " " + str(self.body["start_at"]),
        }

        with open(FILE_PATH, "a") as csv_file:
                w = csv.DictWriter(csv_file, HEADERS)

                if csv_file.tell() == 0:
                    w.writeheader()
                
                w.writerow(writing_data)


    @task
    def say_random(self):
        start_at = time.time()

        sio = socketio.Client()
        ws_url = URL
        try:
            sio.connect(ws_url, transports="websocket")
            self.ws = sio
            self.user_id = sio.sid
            statement = random.choice(self.statements)
            self.body = {"message": statement, "start_at": start_at,"customData": {"language": "en"}, "session_id": self.user_id}
            self.ws.emit('my_event', data=self.body, callback=self.server_returns)

            self.environment.events.request.fire(
                request_type='socket emit',
                name='test/ws/echo',
                response_time=int((time.time() - start_at) * 1000000),
                response_length=len(self.body),
                exception=None,
                context=self.body
            )
        except Exception as e:
            print("**Connection Error**: ", time.time(), " ", e, " ", str(e))
            self.environment.events.request.fire(
                request_type='socket emit',
                name='test/ws/echo',
                response_time=0,
                response_length=0,
                exception=e,
                context=0
            )