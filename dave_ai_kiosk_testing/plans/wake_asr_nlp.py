import csv
import socketio
import time
import string
from random import randint, choice
from datetime import datetime
from locust import task, User
from locust_plugins.csvreader import CSVDictReader

from dave_ai_kiosk_testing.settings import (WAV_AUDIO_PATH, AUDIO_COLLECTION, REPORT_PATH, BACKGROUND_POLLING,
                                            ASR_URL, WAKE_URL, ASR_NLP_SERVER, WAIT_TIME, SSL_VERIFY, RECG, K_MODEL)
from dave_ai_kiosk_testing.config import ASR_NLP_HEADER

# All Audio Utterances
global AUDIO_UTTERANCES
AUDIO_UTTERANCES = {}

# Server URLs
WAKE_SERVER_NAME = "WAKE-UP SERVER " + WAKE_URL
ASR_SERVER_NAME = "ASR SERVER " + ASR_URL

# Reading audio data from a CSV file
try:
    ssn_reader = CSVDictReader(AUDIO_COLLECTION)
except:
    print("Audio data is not available.\n\n")


# Global variable to collect recognition_ids and to stop pooling
global data_rcvd_wake
data_rcvd_wake = []

global data_rcvd_asr
data_rcvd_asr = []


# Request Data Update
REQUEST_DATA_SENT_PATH_WAKE = REPORT_PATH.joinpath("requests_data_sent_wake.csv")
REQUEST_DATA_SENT_PATH_ASR = REPORT_PATH.joinpath("requests_data_sent_asr.csv")
REQUEST_DATA_SENT_PATH_ASR_SINGLE = REPORT_PATH.joinpath("requests_data_sent_asr_single.csv")
REQUEST_DATA_RCVD_PATH_WAKE = REPORT_PATH.joinpath("requests_data_rcvd_wake.csv")
REQUEST_DATA_RCVD_PATH_ASR = REPORT_PATH.joinpath("requests_data_rcvd_asr.csv")


# Running the background task
def background_task_main(sio, uid, recognition_sid, event, count=100):
    for _ in range(count):
        if recognition_sid in data_rcvd_wake and event == "hotwordResults":
            break
        if recognition_sid in data_rcvd_asr and event == "intermediateResults":
            break
        time.sleep(0.1)
        #print(f"Emitting for response: {recognition_sid}")
        if event == "hotwordResults":
            sio.emit(event, data={"uid":uid,"recognition_sid":recognition_sid})
        elif event == "intermediateResults":
            sio.emit("intermediateResults", data={"uid":uid,"sid":recognition_sid})
    
    if recognition_sid in AUDIO_UTTERANCES.keys():
        print("Removing {}".format(recognition_sid))

class ResponseFromServer(socketio.ClientNamespace):
    """
    An event handler to get intermediate results from ASR for audio inputs.
    """
    def on_connect(self):
        pass

    def on_hotword(self, data):
        if data["recognition_sid"] in data_rcvd_wake:
            pass
        else:
            print("WAKE_UP Recognized: ", data)
            data_rcvd_wake.append(data["recognition_sid"])

            # Saving recognized text and response time in a CSV file
            with open(REQUEST_DATA_RCVD_PATH_WAKE, "a") as csv_file:
                w = csv.DictWriter(csv_file, ["component_3","recognition_sid", "wake_rcvd_time", "hotword", "confidence"])
                if csv_file.tell() == 0:
                    w.writeheader()
            
                w.writerow({
                    "component_3": "wake_up",
                    "recognition_sid": data["recognition_sid"],
                    "wake_rcvd_time": time.time(),
                    "hotword": data["rec_text"]["hotword"],
                    "confidence": data["rec_text"]["confidence"],
                })

            # Emitting the recognized wake_up to ASR
            #print("***",AUDIO_UTTERANCES.get(data["recognition_sid"], None))
            if AUDIO_UTTERANCES.get(data["recognition_sid"]):
                # Instantiating local socket client
                sid_tmp = data["recognition_sid"]
                sio_tmp = socketio.Client(ssl_verify=SSL_VERIFY)
                uid_tmp = AUDIO_UTTERANCES[sid_tmp]["new_uid"]
                
                sio_tmp.connect(ASR_URL + "?uid=" +uid_tmp, transports="websocket")
                sio_tmp.emit('astream', data=AUDIO_UTTERANCES[sid_tmp]["audio_data"])
                sio_tmp.emit('astream', data=AUDIO_UTTERANCES[sid_tmp]["stop_audio_data"])
                sio_tmp.register_namespace(ResponseFromServer())
                sio_tmp.start_background_task(background_task_main, sio_tmp, uid_tmp, sid_tmp, "intermediateResults", 100)

                del AUDIO_UTTERANCES[sid_tmp]

                # Saving audio details in a CSV file
                with open(REQUEST_DATA_SENT_PATH_ASR, "a") as csv_file:
                    w = csv.DictWriter(csv_file, ["component_2","recognition_sid", "asr_sent_time"])
                    if csv_file.tell() == 0:
                        w.writeheader()
                    w.writerow({
                        "component_2": "asr",
                        "recognition_sid": sid_tmp,
                        "asr_sent_time": time.time(),
                    })
    
    def on_intermediateResults(self, data):
        if data["is_final"]:
            print("ASR Final Result: ", data["recognition_sid"], data["final_text"])

        # Saving recognized text and response time in a CSV file
        with open(REQUEST_DATA_SENT_PATH_ASR_SINGLE, "a") as csv_file:
            w = csv.DictWriter(csv_file, ["recognition_sid", "asr_alone_rcvd_time",])
            if csv_file.tell() == 0:
                w.writeheader()
            w.writerow({
                "recognition_sid": data["recognition_sid"],
                "asr_alone_rcvd_time": time.time()
            })
            
    def on_convResp(self, data):
        print("NLP Response", data["recognition_sid"], data["conv_resp"].get("customer_state", None))

        data_rcvd_asr.append(data["recognition_sid"])

        # Saving recognized text and response time in a CSV file
        with open(REQUEST_DATA_RCVD_PATH_ASR, "a") as csv_file:
            w = csv.DictWriter(csv_file, ["component_4","recognition_sid", "asr_rcvd_time", "final_text", "customer_state"])
            if csv_file.tell() == 0:
                w.writeheader()
            w.writerow({
                "recognition_sid": data["recognition_sid"],
                "asr_rcvd_time": time.time(),
                "final_text": data["conv_resp"].get("recognized_speech", None),
                "customer_state": data["conv_resp"].get("customer_state", None)
            })

class UserBehaviorWake(User):

    # Wait time between each user instance
    if WAIT_TIME:
        wait_time = WAIT_TIME
    
    # Running the background task
    def background_task(self, uid, recognition_sid, event):
        background_task_main(self.ws, uid, recognition_sid, event)
    
    # Get audio data for load
    def get_audio_data(self, wav_file, recognition_sid, flag=True):
        """
        Function create an audio object to pass the values to ASR server.

        If flag=False, a stopping audio data will be creatted with an emplty blob and 'is_recording' is False.
        By default, flag=True.
        """
        temp = {}
        temp["recognition_sid"] = recognition_sid
        temp["session"] = randint(111111, 999999)
        temp["server"] = ASR_NLP_SERVER
        temp["enterprise_id"] = ASR_NLP_HEADER["ENTERPRISE-ID"]
        temp["conversation_id"] = ASR_NLP_HEADER["CONVERSATION-ID"]
        temp["customer_id"] = ASR_NLP_HEADER["CUSTOMER-ID"]
        temp["engagement_id"] = ASR_NLP_HEADER["ENGAGEMENT-ID"]
        temp["api_key"] = ASR_NLP_HEADER["API-KEY"]
        temp["system_response"] = "sr_init"
        temp["timestamp"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        temp["recognizer"] = RECG
        temp["model_name"] = K_MODEL

        if flag==False:
            temp["blob"] = ""
            temp["is_recording"] = False
            temp["size"] = 0
            return temp

        with open(wav_file, "rb") as f:
            temp["blob"] = f.read()
        
        temp["size"] = len(temp["blob"])
        temp["is_recording"] = True
        return temp

    # Sending the wake-up word first to get response
    @task
    def random_audio_call(self):

        # Selecting a single wav file
        wav_data = next(ssn_reader)
        wav_file_path = WAV_AUDIO_PATH.joinpath(wav_data["file_name"] +".wav")

        # Creating a UID for each connection
        new_uid = ''.join(choice(string.ascii_lowercase) for _ in range(5))

        # Instantiating local socket client
        self.ws = socketio.Client(ssl_verify=SSL_VERIFY)

        # Recongnition sid for audio start and stop
        self.recognition_sid = "".join([choice(string.digits) for i in range(8)])

        # Getting audio_data from a WAV file
        self.audio_data = self.get_audio_data(wav_file_path, recognition_sid = self.recognition_sid)
        self.stop_audio_data = self.get_audio_data("", recognition_sid = self.recognition_sid, flag=False)

        # Counting start time to record the response time
        start_at = time.time()

        try:
            self.ws.connect(WAKE_URL + "?uid=" +new_uid, transports="websocket")
            self.ws.register_namespace(ResponseFromServer())
            self.ws.emit('astream', data=self.audio_data)
            self.ws.emit('astream', data=self.stop_audio_data)

            # Storing the audio utterances
            AUDIO_UTTERANCES[self.recognition_sid] = { "new_uid":new_uid, "audio_data": self.audio_data, "stop_audio_data": self.stop_audio_data}

            # Saving audio details in a CSV file
            with open(REQUEST_DATA_SENT_PATH_WAKE, "a") as csv_file:
                w = csv.DictWriter(csv_file, ["component_1","recognition_sid", "utterance", "file_name", "wake_sent_time", "wake_up_word"])
                if csv_file.tell() == 0:
                    w.writeheader()
                w.writerow({
                    "component_1": "wake_up",
                    "recognition_sid": self.recognition_sid,
                    "utterance": wav_data["utterance"],
                    "file_name": wav_data["file_name"],
                    "wake_sent_time": time.time(),
                    "wake_up_word": wav_data["wake_up_word"]
                })
            print("WAKE_UP: {}: {}".format(self.recognition_sid, wav_data["utterance"]))
            
            if BACKGROUND_POLLING == True:
                self.ws.start_background_task(self.background_task, new_uid, self.audio_data["recognition_sid"], "hotwordResults")
            else:
                self.background_task(new_uid, self.audio_data["recognition_sid"], "hotwordResults")
            self.environment.events.request.fire(
                request_type='Websocket',
                name=WAKE_SERVER_NAME,
                response_time=(time.time() - start_at),
                response_length=len(self.audio_data),
                exception=None,
                context=self.audio_data
            )
        except Exception as e:
            print("Connection Error: ", time.time(), " ", e, " ", str(e))
            self.environment.events.request.fire(
                request_type='Websocket',
                name=ASR_SERVER_NAME,
                response_time=0,
                response_length=0,
                exception=e,
                context=0
            )