import csv
import random
import socketio
import time
import string
from random import randint, choice
from datetime import datetime
from locust import task, User, constant
from locust_plugins.csvreader import CSVDictReader
from base64 import b64encode

from dave_ai_kiosk_testing.settings import (REPORT_PATH, WAIT_TIME, SSL_VERIFY, IMAGE_PRO_URL, IMAGE_PRO_URL_2, 
                                            IMAGE_DATA_PATH, IMAGE_COLLECTION, IMAGE_COLLECTION2, IMAGE_CLASSIFIER, IMAGE_FRAME_RATE)

# Reading audio data from a CSV file
try:
    ssn_reader = CSVDictReader(IMAGE_COLLECTION)
    ssn_reader2 = CSVDictReader(IMAGE_COLLECTION2)
except:
    print("Image data is not available.\n\n")

# Global variable to collect recognition_ids and to stop pooling
global data_rcvd
data_rcvd = []


# Request Data Update
REQUEST_DATA_SENT_PATH = REPORT_PATH.joinpath("requests_data_sent_image.csv")
REQUEST_DATA_RCVD_PATH = REPORT_PATH.joinpath("requests_data_rcvd_image.csv")


class ResponseFromServer(socketio.ClientNamespace):
    """
    An event handler to get intermediate results from Image Processing server for image inputs.
    """
    def on_connect(self):
        pass

    def on_intermediateClassifierResults(self, data):
        if data["is_final"] == False:
            print("Result: ",data)
            data_rcvd.append(data["sid"])

            with open(REQUEST_DATA_RCVD_PATH, "a") as csv_file:
                w = csv.DictWriter(csv_file, ["recognition_sid", "rcvd_time", "result", "confidence"])
                if csv_file.tell() == 0:
                    w.writeheader()
                if data["classifier_result"]:
                    classifier_result = data["classifier_result"][0]
                else:
                    classifier_result = {}
                w.writerow({
                        "recognition_sid": data["sid"],
                        "rcvd_time": time.time(),
                        "result": classifier_result.get("Result", ""),
                        "confidence": classifier_result.get("confidence", 0)
                        })

class UserBehaviorImageFace(User):

    # Wait time between each user instance
    wait_time = constant(IMAGE_FRAME_RATE)
    
    # Running the background taskq
    def background_task(self, uid, recognition_sid):
        for _ in range(100):
            if recognition_sid in data_rcvd:
                break
            time.sleep(0.1)
            print(f"Emitting for response: {recognition_sid}")
            self.ws.emit('intermediateClassifierResults', data={"uid":uid,"sid":recognition_sid})
    
    # Get audio data for load
    def get_image_data(self, img_file, recognition_sid, classifier=IMAGE_CLASSIFIER,flag=True):
        """
        Function create an image object to pass the values to Image Processing server.

        If flag=False, a stopping image data will be creatted with an emplty data and 'is_recording' is False.
        By default, flag=True.
        """
        temp = {}
        temp["sid"] = recognition_sid

        if flag==False:
            temp["data"] = ""
            temp["is_recording"] = False
            return temp
        
        #temp["data"] = '[enterprise_id][conversation_id][customer_id][engagement_id][api_key][system_response][timestamp]||||{"recognizer_type":"selectedRecognizer", "classNames":["LicenseNumberDetector"]}||||data:image/jpeg;base64,'
        #temp["data"] = '[enterprise_id][conversation_id][customer_id][engagement_id][api_key][system_response][timestamp]||||{"recognizer_type":"selectedRecognizer", "classNames":["FaceDetectorCafe"]}||||data:image/jpeg;base64,'
        temp["data"] = '[enterprise_id][conversation_id][customer_id][engagement_id][api_key][system_response][timestamp]||||{"recognizer_type":"selectedRecognizer", "classNames":["'
        temp["data"] = temp["data"] + classifier
        temp["data"] = temp["data"] + '"]}||||data:image/jpeg;base64,'
        with open(img_file, 'rb') as f:
            temp["data"] = temp["data"] + b64encode(f.read()).decode("utf-8")

        temp["data"] = temp["data"] + '||||'

        temp["is_recording"] = True
        return temp

    @task
    def random_face_recognition(self):

        print("Started sending face images...")

        # Selecting a single wav file
        img_data = next(ssn_reader)
        img_file_path = IMAGE_DATA_PATH.joinpath(img_data["file_name"] +".jpg")

        # Creating UID for each connection
        new_uid = ''.join(choice(string.ascii_lowercase) for _ in range(5))

        # Instantiating local socket client
        self.ws = socketio.Client(ssl_verify=SSL_VERIFY)

        # Recongnition sid for audio start and stop
        self.recognition_sid = "".join([choice(string.digits) for i in range(8)])

        # Getting audio_data from a WAV file
        image_data = self.get_image_data(img_file_path, recognition_sid = self.recognition_sid, classifier="FaceDetectorCafe")
        stop_image_data = self.get_image_data("", recognition_sid = self.recognition_sid, flag=False)

        # Counting start time to record the response time
        start_at = time.time()

        try:
            self.ws.connect(IMAGE_PRO_URL + "?uid=" +new_uid, transports="websocket")
            self.ws.register_namespace(ResponseFromServer())
            self.ws.start_background_task(self.background_task, new_uid, self.recognition_sid)
            self.ws.emit('imageStream', data=image_data)
            self.ws.emit('imageStream', data=stop_image_data)

            # Saving audio details in a CSV file
            with open(REQUEST_DATA_SENT_PATH, "a") as csv_file:
                w = csv.DictWriter(csv_file, ["recognition_sid", "file_name", "label", "sent_time"])
                if csv_file.tell() == 0:
                    w.writeheader()
                w.writerow({
                    "recognition_sid": self.recognition_sid,
                    "file_name": img_data["file_name"],
                    "label": img_data["label"],
                    "sent_time": time.time()
                })
            self.environment.events.request.fire(
                request_type='Websocket',
                name="Image Processing Server [Face]",
                response_time=(time.time() - start_at),
                response_length=len(image_data),
                exception=None,
                context=image_data
            )
        except Exception as e:
            print("Connection Error: ", time.time(), " ", e, " ", str(e))
            self.environment.events.request.fire(
                request_type='Websocket',
                name="Image Processing Server [Face]",
                response_time=0,
                response_length=0,
                exception=e,
                context=0
            )


class UserBehaviorImageVehicle(User):

    # Wait time between each user instance
    wait_time = constant(IMAGE_FRAME_RATE)
    
    # Running the background taskq
    def background_task(self, uid, recognition_sid):
        for _ in range(100):
            if recognition_sid in data_rcvd:
                break
            time.sleep(0.1)
            print(f"Emitting for response: {recognition_sid}")
            self.ws.emit('intermediateClassifierResults', data={"uid":uid,"sid":recognition_sid})
    
    # Get audio data for load
    def get_image_data(self, img_file, recognition_sid, classifier=IMAGE_CLASSIFIER,flag=True):
        """
        Function create an image object to pass the values to Image Processing server.

        If flag=False, a stopping image data will be creatted with an emplty data and 'is_recording' is False.
        By default, flag=True.
        """
        temp = {}
        temp["sid"] = recognition_sid

        if flag==False:
            temp["data"] = ""
            temp["is_recording"] = False
            return temp
        
        #temp["data"] = '[enterprise_id][conversation_id][customer_id][engagement_id][api_key][system_response][timestamp]||||{"recognizer_type":"selectedRecognizer", "classNames":["LicenseNumberDetector"]}||||data:image/jpeg;base64,'
        #temp["data"] = '[enterprise_id][conversation_id][customer_id][engagement_id][api_key][system_response][timestamp]||||{"recognizer_type":"selectedRecognizer", "classNames":["FaceDetectorCafe"]}||||data:image/jpeg;base64,'
        temp["data"] = '[enterprise_id][conversation_id][customer_id][engagement_id][api_key][system_response][timestamp]||||{"recognizer_type":"selectedRecognizer", "classNames":["'
        temp["data"] = temp["data"] + classifier
        temp["data"] = temp["data"] + '"]}||||data:image/jpeg;base64,'
        with open(img_file, 'rb') as f:
            temp["data"] = temp["data"] + b64encode(f.read()).decode("utf-8")

        temp["data"] = temp["data"] + '||||'

        temp["is_recording"] = True
        return temp
    
    @task
    def random_vehicle_recognition(self):

        print("Started sending face images...")

        # Selecting a single wav file
        img_data = next(ssn_reader2)
        img_file_path = IMAGE_DATA_PATH.joinpath(img_data["file_name"] +".jpg")

        # Creating UID for each connection
        new_uid = ''.join(choice(string.ascii_lowercase) for _ in range(5))

        # Instantiating local socket client
        self.ws = socketio.Client(ssl_verify=SSL_VERIFY)

        # Recongnition sid for audio start and stop
        self.recognition_sid = "".join([choice(string.digits) for i in range(8)])

        # Getting audio_data from a WAV file
        image_data = self.get_image_data(img_file_path, recognition_sid = self.recognition_sid, classifier="LicenseNumberDetector")
        stop_image_data = self.get_image_data("", recognition_sid = self.recognition_sid, flag=False)

        # Counting start time to record the response time
        start_at = time.time()

        try:
            self.ws.connect(IMAGE_PRO_URL_2 + "?uid=" +new_uid, transports="websocket")
            self.ws.register_namespace(ResponseFromServer())
            self.ws.start_background_task(self.background_task, new_uid, self.recognition_sid)
            self.ws.emit('imageStream', data=image_data)
            self.ws.emit('imageStream', data=stop_image_data)

            # Saving audio details in a CSV file
            with open(REQUEST_DATA_SENT_PATH, "a") as csv_file:
                w = csv.DictWriter(csv_file, ["recognition_sid", "file_name", "label", "sent_time"])
                if csv_file.tell() == 0:
                    w.writeheader()
                w.writerow({
                    "recognition_sid": self.recognition_sid,
                    "file_name": img_data["file_name"],
                    "label": img_data["label"],
                    "sent_time": time.time()
                })
            self.environment.events.request.fire(
                request_type='Websocket',
                name="Image Processing Server [Vehicle]",
                response_time=(time.time() - start_at),
                response_length=len(image_data),
                exception=None,
                context=image_data
            )
        except Exception as e:
            print("Connection Error: ", time.time(), " ", e, " ", str(e))
            self.environment.events.request.fire(
                request_type='Websocket',
                name="Image Processing Server [Vehicle]",
                response_time=0,
                response_length=0,
                exception=e,
                context=0
            )













"""

I have to send the data to imagestream event.

data format is in template/ImageProcessing.html. Check dinesh text
"""