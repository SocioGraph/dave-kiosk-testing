import csv
import json
import time
import string
from locust import HttpUser, task
from locust.user.wait_time import constant
from locust_plugins.csvreader import CSVDictReader
from random import choice

from dave_ai_kiosk_testing.settings import REPORT_PATH, TEXT_COLLECTION,SSL_VERIFY, NLP_MAIN_URL, WAIT_TIME, TEMP_DIR, TEMP_USR_CSV
from dave_ai_kiosk_testing.config import NLP_HEADER, NLP_CONV_ID, NLP_CUST_ID, NLP_ENG_ID

# Request Data Update
REQUEST_DATA_EXCH_PATH = REPORT_PATH.joinpath("requests_data_exchange.csv")

ssn_reader = CSVDictReader(TEXT_COLLECTION)

user_reader = CSVDictReader(TEMP_DIR.joinpath(TEMP_USR_CSV))

class UserBehavior(HttpUser):

    host = NLP_MAIN_URL
    
    # Wait time between each requests
    if WAIT_TIME:
        wait_time = WAIT_TIME
    
    @task
    def random_conversation(self):

        # Generating conversation text
        random_conv = next(ssn_reader)

        # User info
        random_user = next(user_reader)

        # Creating a payload to start conversation 
        nlp_payload = {
            "customer_response": random_conv["utterance"],
            "system_response": "sr_init",
            "csv_response": False, # True:  Only when I am caching... Else False
            "refresh_cache": False,
            "engagement_id": random_user["enterprise_id"]
        }
                
        nlp_header = {
            "X-I2CE-API-KEY": random_user["api_key"],
            "X-I2CE-USER-ID": random_user["user_id"],
            "X-I2CE-ENTERPRISE-ID": random_user["enterprise_id"],
            "Content-Type": "application/json"
        }

        nlp_url = NLP_MAIN_URL + "/conversation/" + NLP_CONV_ID + "/" + random_user["user_id"]

        start_at = time.time()

        with self.client.post(nlp_url, headers=nlp_header, data=json.dumps(nlp_payload), catch_response=True, verify=SSL_VERIFY) as response:
            resp_json = response.json()
            print("*Conversation*")
            print("Original Customer State: ",random_conv["customer_state"])
            print("Customer Response: ",random_conv["utterance"])
            print("\n")
            print("Predicted Customer state: ",resp_json["customer_state"])
            print("System Response: ",resp_json["placeholder"])
            print("\n\n")

            # Saving data exchange in a CSV file
            with open(REQUEST_DATA_EXCH_PATH, "a") as csv_file:
                w = csv.DictWriter(csv_file, ["original_cs", "original_utterance", "nlp_detect_cs", "placeholder","response_start_time","response_got_time","response_time"])
                if csv_file.tell() == 0:
                    w.writeheader()
                w.writerow({
                    "original_cs": random_conv["customer_state"],
                    "original_utterance": random_conv["utterance"],
                    "nlp_detect_cs": resp_json["customer_state"],
                    "placeholder": resp_json["placeholder"],
                    "response_start_time":start_at,
                    "response_got_time": time.time(),
                    "response_time": (time.time()-start_at),
                })