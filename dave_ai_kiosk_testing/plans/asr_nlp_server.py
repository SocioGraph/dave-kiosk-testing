import csv
import random
import socketio
import time
import string
from random import randint, choice
from datetime import datetime
from locust import task, User
from locust_plugins.csvreader import CSVDictReader

from dave_ai_kiosk_testing.settings import (WAV_AUDIO_PATH, AUDIO_COLLECTION, REPORT_PATH, ASR_URL, BACKGROUND_POLLING,
                                            WAIT_TIME, ASR_NLP_SERVER, K_MODEL, RECG, SSL_VERIFY)
from dave_ai_kiosk_testing.config import ASR_NLP_HEADER

# ASR Server URL
SERVER_NAME = "ASR SERVER " + ASR_URL

# Reading audio data from a CSV file
try:
    ssn_reader = CSVDictReader(AUDIO_COLLECTION)
except:
    print("Audio data is not available.\n\n")


# Global variable to collect recognition id
global data_rcvd
data_rcvd = []


# Request Data Update
REQUEST_DATA_SENT_PATH = REPORT_PATH.joinpath("requests_data_sent.csv")
REQUEST_DATA_RCVD_PATH = REPORT_PATH.joinpath("requests_data_rcvd.csv")


class ResponseFromServer(socketio.ClientNamespace):
    """
    An event handler to get intermediate results from ASR for audio inputs.
    """
    def on_connect(self):
        pass

    def on_intermediateResults(self, data):
        if data["is_final"]:
            print("ASR IntermediateResults: ", data["recognition_sid"], data["final_text"])
    
    def on_convResp(self, data):
        print("NLP Response", data["recognition_sid"], data["conv_resp"].get("customer_state", None))

        data_rcvd.append(data["recognition_sid"])

        # Saving recognized text and response time in a CSV file
        with open(REQUEST_DATA_RCVD_PATH, "a") as csv_file:
            w = csv.DictWriter(csv_file, ["recognition_sid", "rcvd_time", "final_text", "customer_state"])
            if csv_file.tell() == 0:
                w.writeheader()
            w.writerow({
                "recognition_sid": data["recognition_sid"],
                "rcvd_time": time.time(),
                "final_text": data["conv_resp"].get("recognized_speech", None),
                "customer_state": data["conv_resp"].get("customer_state", None)
            })

class UserBehavior(User):

    # Wait time between each user instance
    if WAIT_TIME:
        wait_time = WAIT_TIME
    
    # Running the background task
    def background_task(self, uid, recognition_sid, poll_count=1000):
        for _ in range(poll_count):
            if recognition_sid in data_rcvd:
                break
            time.sleep(0.05)
            print(f"Emitting for response: {recognition_sid}")
            self.ws.emit('intermediateResults', data={"uid":uid,"sid":recognition_sid})
    
    # Get audio data for load
    def get_audio_data(self, wav_file, recognition_sid, flag=True):
        """
        Function create an audio object to pass the values to ASR server.

        If flag=False, a stopping audio data will be creatted with an emplty blob and 'is_recording' is False.
        By default, flag=True.
        """
        temp = {}
        temp["recognition_sid"] = recognition_sid
        temp["session"] = randint(111111, 999999)
        temp["server"] = ASR_NLP_SERVER
        temp["enterprise_id"] = ASR_NLP_HEADER["ENTERPRISE-ID"]
        temp["conversation_id"] = ASR_NLP_HEADER["CONVERSATION-ID"]
        temp["customer_id"] = ASR_NLP_HEADER["CUSTOMER-ID"]
        temp["engagement_id"] = ASR_NLP_HEADER["ENGAGEMENT-ID"]
        temp["api_key"] = ASR_NLP_HEADER["API-KEY"]
        temp["system_response"] = "sr_init"
        temp["timestamp"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        temp["recognizer"] = RECG
        temp["model_name"] = K_MODEL

        if flag==False:
            temp["blob"] = ""
            temp["is_recording"] = False
            temp["size"] = 0
            return temp

        with open(wav_file, "rb") as f:
            temp["blob"] = f.read()

        temp["size"] = len(temp["blob"])
        temp["is_recording"] = True
        
        return temp
        

    @task
    def random_audio_call(self):

        # Selecting a single wav file
        wav_data = next(ssn_reader)
        wav_file_path = WAV_AUDIO_PATH.joinpath(wav_data["file_name"] +".wav")

        # Creating UID for each connection
        new_uid = ''.join(choice(string.ascii_lowercase) for _ in range(5))

        # Instantiating local socket client
        self.ws = socketio.Client(ssl_verify=SSL_VERIFY)

        # Recongnition sid for audio start and stop
        self.recognition_sid = "".join([choice(string.digits) for i in range(8)])

        # Getting audio_data from a WAV file
        audio_data = self.get_audio_data(wav_file_path, recognition_sid = self.recognition_sid)
        stop_audio_data = self.get_audio_data("", recognition_sid = self.recognition_sid, flag=False)
        
        # Counting start time to record the response time
        start_at = time.time()

        try:
            self.ws.connect(ASR_URL + "?uid=" +new_uid, transports="websocket")
            self.ws.register_namespace(ResponseFromServer())
            self.ws.emit('astream', data=audio_data)
            self.ws.emit('astream', data=stop_audio_data)
            if BACKGROUND_POLLING == True:
                self.ws.start_background_task(self.background_task, new_uid, self.recognition_sid, 1000)
            else:
                self.background_task(new_uid, self.recognition_sid)
            
            # Saving audio details in a CSV file
            with open(REQUEST_DATA_SENT_PATH, "a") as csv_file:
                w = csv.DictWriter(csv_file, ["utterance", "key", "recognition_sid","sent_time","duration","file_name"])
                if csv_file.tell() == 0:
                    w.writeheader()
                w.writerow({
                    "utterance": wav_data["utterance"],
                    "key": wav_data["key"],
                    "recognition_sid": self.recognition_sid,
                    "sent_time": time.time(),
                    "duration": wav_data["duration"],
                    "file_name": wav_data["file_name"]
                })
            
            self.environment.events.request.fire(
                request_type='Websocket',
                name=SERVER_NAME,
                response_time=(time.time() - start_at),
                response_length=len(audio_data),
                exception=None,
                context=audio_data
            )
        except Exception as e:
            print("Connection Error: ", time.time(), " ", e, " ", str(e))
            self.environment.events.request.fire(
                request_type='Websocket',
                name=SERVER_NAME,
                response_time=0,
                response_length=0,
                exception=e,
                context=0
            )