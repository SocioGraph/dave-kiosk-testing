import csv
import os
import json
import socketio
import time
import string
import skvideo.io
from skimage.io import imsave
from random import randint, choice
from datetime import datetime
from locust import task, User
from locust_plugins.csvreader import CSVDictReader
from base64 import b64encode

from dave_ai_kiosk_testing.settings import REPORT_PATH, WAIT_TIME, SSL_VERIFY, IMAGE_PRO_URL, IMAGE_DATA_PATH, IMAGE_CLASSIFIER, TEMP_DIR, VIDEO_DATA_PATH, VIDEO_COLLECTION

def frame_gen(videogen, true_val):
    for i, frame in enumerate(videogen):
        i = i+1
        if i % 12 == 0:
            yield((frame, true_val[i]))


# Reading audio data from a CSV file
try:
    with open(VIDEO_COLLECTION, "r") as f:
        video_collection = json.load(f)

    videogen = skvideo.io.vreader(video_collection["face_detection"]["test_file_name"])
    true_data = video_collection["face_detection"]["true_data"]

    ssn_reader = frame_gen(videogen, true_data)
except:
    print("Image data is not available.\n\n")



# Global variable to collect recognition_ids and to stop pooling
global data_rcvd
data_rcvd = []


# Request Data Update
REQUEST_DATA_SENT_PATH = REPORT_PATH.joinpath("requests_data_sent.csv")
REQUEST_DATA_RCVD_PATH = REPORT_PATH.joinpath("requests_data_rcvd.csv")


class ResponseFromServer(socketio.ClientNamespace):
    """
    An event handler to get intermediate results from Image Processing server for image inputs.
    """
    def on_connect(self):
        pass

    def on_intermediateClassifierResults(self, data):
        if data["is_final"] == False:
            print("Result: ",data)
            data_rcvd.append(data["sid"])

            with open(REQUEST_DATA_RCVD_PATH, "a") as csv_file:
                w = csv.DictWriter(csv_file, ["recognition_sid", "rcvd_time", "result", "confidence"])
                if csv_file.tell() == 0:
                    w.writeheader()
                if data["classifier_result"]:
                    classifier_result = data["classifier_result"][0]
                else:
                    classifier_result = {}
                w.writerow({
                        "recognition_sid": data["sid"],
                        "rcvd_time": time.time(),
                        "result": classifier_result.get("Result", ""),
                        "confidence": classifier_result.get("confidence", 0)
                        })

class UserBehavior(User):

    # Wait time between each user instance
    if WAIT_TIME:
        wait_time = WAIT_TIME
    
    # Running the background task
    def background_task(self, uid, recognition_sid):
        for _ in range(500):
            if recognition_sid in data_rcvd:
                break
            time.sleep(0.01)
            print(f"Emitting for response: {recognition_sid}")
            self.ws.emit('intermediateClassifierResults', data={"uid":uid,"sid":recognition_sid})
    
    # Get audio data for load
    def get_image_data(img_array, recognition_sid, flag=True, recog_class="FaceDetectorCafe"):
        """
        Function create an image object to pass the values to Image Processing server.

        If flag=False, a stopping image data will be creatted with an emplty data and 'is_recording' is False.
        By default, flag=True.
        """
        temp = {}
        temp["sid"] = recognition_sid
        temp["is_recording"] = True

        if flag==False:
            temp["data"] = ""
            temp["is_recording"] = False
            return temp
        
        # saving temp image
        imsave(TEMP_DIR.joinpath("temp.png"), img_array)

        #temp["data"] = '[enterprise_id][conversation_id][customer_id][engagement_id][api_key][system_response][timestamp]||||{"recognizer_type":"selectedRecognizer", "classNames":["LicenseNumberDetector"]}||||data:image/jpeg;base64,'
        #temp["data"] = '[enterprise_id][conversation_id][customer_id][engagement_id][api_key][system_response][timestamp]||||{"recognizer_type":"selectedRecognizer", "classNames":["FaceDetectorCafe"]}||||data:image/jpeg;base64,'
        temp["data"] = '[enterprise_id][conversation_id][customer_id][engagement_id][api_key][system_response][timestamp]||||{"recognizer_type":"selectedRecognizer", "classNames":["'
        temp["data"] = temp["data"] + recog_class
        temp["data"] = temp["data"] + '"]}||||data:image/jpeg;base64,'
        with open("temp.png", "rb") as frame_img:
            temp["data"] = temp["data"] + b64encode(frame_img.read()).decode("utf-8")
        temp["data"] = temp["data"] + '||||'

        os.remove(TEMP_DIR.joinpath("temp.png"))
        return temp

    @task
    def random_image_recognition(self):

        print("Started taking images...")

        # Selecting a single wav file
        img_data = next(ssn_reader)
        img_array = img_data[0]
        true_val = img_data[1]

        # Creating UID for each connection
        new_uid = ''.join(choice(string.ascii_lowercase) for _ in range(5))

        # Instantiating local socket client
        self.ws = socketio.Client(ssl_verify=SSL_VERIFY)

        # Recongnition sid for audio start and stop
        self.recognition_sid = "".join([choice(string.digits) for i in range(8)])

        # Getting audio_data from a WAV file
        image_data = self.get_image_data(img_array, recognition_sid = self.recognition_sid)
        stop_image_data = self.get_image_data("", recognition_sid = self.recognition_sid, flag=False)

        # Counting start time to record the response time
        start_at = time.time()

        try:
            self.ws.connect(IMAGE_PRO_URL + "?uid=" +new_uid, transports="websocket")
            self.ws.register_namespace(ResponseFromServer())
            self.ws.start_background_task(self.background_task, new_uid, self.recognition_sid)
            self.ws.emit('imageStream', data=image_data)
            self.ws.emit('imageStream', data=stop_image_data)

            # Saving audio details in a CSV file
            with open(REQUEST_DATA_SENT_PATH, "a") as csv_file:
                w = csv.DictWriter(csv_file, ["recognition_sid", "label", "sent_time"])
                if csv_file.tell() == 0:
                    w.writeheader()
                w.writerow({
                    "recognition_sid": self.recognition_sid,
                    "label": true_val,
                    "sent_time": time.time()
                })
            self.environment.events.request.fire(
                request_type='Websocket',
                name="Image Processing Server",
                response_time=(time.time() - start_at),
                response_length=len(image_data),
                exception=None,
                context=image_data
            )
        except Exception as e:
            print("Connection Error: ", time.time(), " ", e, " ", str(e))
            self.environment.events.request.fire(
                request_type='Websocket',
                name="Image Processing Server",
                response_time=0,
                response_length=0,
                exception=e,
                context=0
            )



















"""

I have to send the data to imagestream event.

data format is in template/ImageProcessing.html. Check dinesh text
"""