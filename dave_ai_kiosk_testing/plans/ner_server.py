import time
from locust import HttpUser, task
from locust_plugins.csvreader import CSVDictReader
import random

from dave_ai_kiosk_testing.settings import REPORT_PATH, TEXT_COLLECTION

# NLP Server URL
NER_URL = "http://localhost:5052"

ssn_reader = CSVDictReader(TEXT_COLLECTION)

WAIT_TIME = None

class UserBehavior(HttpUser):

    host = NER_URL
    
    # Wait time between each requests
    if WAIT_TIME:
        wait_time = WAIT_TIME

    @task
    def random_conversation(self):
        # Generating randome text for load testing
        random_text = next(ssn_reader)
        random_conv = "/tags?q=" + random_text["random text"]
        
        with self.client.get(random_conv, catch_response=True) as response:
            print(response.status_code)
            with open(REPORT_PATH.joinpath("nlp_response.txt"), "a") as txt:
                txt.write(response.text)
                txt.write("\n\n")

"""


"""