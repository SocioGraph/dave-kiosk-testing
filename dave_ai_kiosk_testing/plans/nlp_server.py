import csv
import json
import time
import string
from locust import HttpUser, task
from locust.user.wait_time import constant
from locust_plugins.csvreader import CSVDictReader
from random import choice

from dave_ai_kiosk_testing.settings import REPORT_PATH, TEXT_COLLECTION,SSL_VERIFY, NLP_MAIN_URL, WAIT_TIME
from dave_ai_kiosk_testing.config import NLP_HEADER, NLP_CONV_ID, NLP_CUST_ID, NLP_ENG_ID

# Request Data Update
REQUEST_DATA_EXCH_PATH = REPORT_PATH.joinpath("requests_data_exchange.csv")

ssn_reader = CSVDictReader(TEXT_COLLECTION)

NLP_URL = NLP_MAIN_URL + "/conversation/" + NLP_CONV_ID + "/" + NLP_CUST_ID

class UserBehavior(HttpUser):

    host = NLP_URL
    
    # Wait time between each requests
    if WAIT_TIME:
        wait_time = WAIT_TIME
    
    @task
    def random_conversation(self):

        # Generating conversation text
        random_conv = next(ssn_reader)

        # Creating a payload to start conversation 
        nlp_payload = {}
        nlp_payload["customer_response"] = random_conv["utterance"]
        nlp_payload["system_response"] = "sr_init"
        nlp_payload["refresh_cache"] = False
        if NLP_ENG_ID:
            nlp_payload["engagement_id"] = NLP_ENG_ID
        
        start_at = time.time()

        with self.client.post(NLP_URL, headers=NLP_HEADER, data=json.dumps(nlp_payload), catch_response=True, verify=SSL_VERIFY) as response:
            resp_json = response.json()
            print("*Conversation*")
            print("Original Customer State: ",random_conv["customer_state"])
            print("Customer Response: ",random_conv["utterance"])
            print("\n")
            print("Predicted Customer state: ",resp_json["customer_state"])
            print("System Response: ",resp_json["placeholder"])
            print("\n\n")

            # Saving data exchange in a CSV file
            with open(REQUEST_DATA_EXCH_PATH, "a") as csv_file:
                w = csv.DictWriter(csv_file, ["original_cs", "original_utterance", "nlp_detect_cs", "placeholder","response_start_time","response_got_time","response_time"])
                if csv_file.tell() == 0:
                    w.writeheader()
                w.writerow({
                    "original_cs": random_conv["customer_state"],
                    "original_utterance": random_conv["utterance"],
                    "nlp_detect_cs": resp_json["customer_state"],
                    "placeholder": resp_json["placeholder"],
                    "response_start_time":start_at,
                    "response_got_time": time.time(),
                    "response_time": (time.time()-start_at),
                })