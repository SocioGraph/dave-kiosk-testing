# Importing basic libraries
import sys
import logging
import os
import time
from datetime import date, datetime

# Locust wait time
from locust.user.wait_time import constant

# Importing setting variables
from . import settings, config

#
from .utils2 import test_set_collection
from .utils import create_users, clean_repport_dir, created_heading

# Importing locust local runner function 
from .run import run_component

def main(**kwargs):

    # Updating parameter required for tests
    settings.SSL_VERIFY = kwargs["SSL_VERIFY"]
    settings.WAIT_TIME = constant(kwargs["WAIT"])
    settings.K_MODEL = kwargs["K_MODEL"]
    settings.RECG = kwargs["RECG"]
    settings.CHUNK_SIZE = kwargs["CHUNK_SIZE"]
    settings.BACKGROUND_POLLING = kwargs["POOL_BACK"]
    settings.IMAGE_FRAME_RATE = kwargs["IMAGE_FRAME"]

    # Creating a 
    kwargs["MODIFIED_RUN_TIME"] = 30

    # Checking for the domain
    if kwargs["DOMAIN"] == None and kwargs["COMPONENT"] != "IDLE":
        print("Enter the domain before running the test.")
        time.sleep(2)
        sys.exit()

    if kwargs["COMPONENT"] == "ASR":

        # Updating ASR URL
        settings.ASR_URL = kwargs["URL"][0]

        # Loading audio data
        settings.AUDIO_COLLECTION = settings.AUDIO_DATA_PATH.joinpath(test_set_collection("audio", kwargs["DOMAIN"]))

        if (os.path.isfile(settings.AUDIO_COLLECTION)):
            sys.stderr.write("Audio data is available for load testing.\n\n")
        else:
            from dave_ai_kiosk_testing.data.load_data import get_audio_data
            sys.stderr.write("Audio data is not available. Will be downloaded from the server.\n\n")
            get_audio_data(kwargs["DOMAIN"])

        # Loading ASR server locust class
        from dave_ai_kiosk_testing.plans import asr_server, asr_server_chunks
        
        if kwargs["WARMUP"] == True:
            print(created_heading("Running a warmup session.", "-"))
            if kwargs["CHUNKS"]:
                # Running ASR component with chunks
                run_component(user_classes=[asr_server_chunks.UserBehavior], **kwargs)
            else:
                # Running ASR component
                run_component(user_classes=[asr_server.UserBehavior], **kwargs)
            print(created_heading("Warmup session is done. The load testing will start.", "-"))
            kwargs["WARMUP"] = False
            clean_repport_dir()

        if kwargs["CHUNKS"]:
            # Running ASR component with chunks
            run_component(user_classes=[asr_server_chunks.UserBehavior], **kwargs)
        else:
            # Running ASR component
            run_component(user_classes=[asr_server.UserBehavior], **kwargs)


    elif kwargs["COMPONENT"] == "WAKE":

        # Updating ASR URL
        settings.ASR_URL = kwargs["URL"][0]

        # Loading audio data
        settings.AUDIO_COLLECTION = settings.AUDIO_DATA_PATH.joinpath(test_set_collection("audio", kwargs["DOMAIN"]))

        if (os.path.isfile(settings.AUDIO_COLLECTION)):
            print("Audio data is available for load testing.\n\n")
        else:
            from dave_ai_kiosk_testing.data.load_data import get_audio_data
            print("Audio data is not available. Will be downloaded from the server.\n\n")
            get_audio_data(kwargs["DOMAIN"])

        # Loading ASR server locust class
        from dave_ai_kiosk_testing.plans import wake
        
        if kwargs["WARMUP"] == True:
            print(created_heading("Running a warmup session.", "-"))
            run_component(user_classes=[wake.UserBehavior], **kwargs)
            print(created_heading("Warmup session is done. The load testing will start.", "-"))
            kwargs["WARMUP"] = False
            clean_repport_dir()

        # Running ASR component
        run_component(user_classes=[wake.UserBehavior], **kwargs)    
    
    elif kwargs["COMPONENT"] == "WAKE_ASR":

        # Updating ASR URL
        settings.WAKE_URL = kwargs["URL"][0]
        settings.ASR_URL = kwargs["URL"][1]

        # Loading audio data
        settings.AUDIO_COLLECTION = settings.AUDIO_DATA_PATH.joinpath(test_set_collection("audio", kwargs["DOMAIN"]))

        if (os.path.isfile(settings.AUDIO_COLLECTION)):
            print("Audio data is available for load testing.\n\n")
        else:
            from dave_ai_kiosk_testing.data.load_data import get_audio_data
            print("Audio data is not available. Will be downloaded from the server.\n\n")
            get_audio_data(kwargs["DOMAIN"])

        # Loading ASR server locust class
        from dave_ai_kiosk_testing.plans import wake_asr
        
        # Running WAKE_ASR component
        if kwargs["WARMUP"] == True:
            print(created_heading("Running a warmup session.", "-"))
            run_component(user_classes=[wake_asr.UserBehaviorWake], **kwargs)
            print(created_heading("Warmup session is done. The load testing will start.", "-"))
            kwargs["WARMUP"] = False
            clean_repport_dir()
                
        run_component(user_classes=[wake_asr.UserBehaviorWake], **kwargs)

    
    elif kwargs["COMPONENT"] == "NLP":
        
        # Loading text data
        settings.TEXT_COLLECTION = settings.TEXT_DATA_PATH.joinpath(test_set_collection("text", kwargs["DOMAIN"]))

        # Updating conversation_id
        config.NLP_CONV_ID = kwargs["CONV_ID"]

        # Updating some NLP arguments
        settings.NLP_MAIN_URL = kwargs["URL"][0]

        if kwargs["MULTI_USER"]:

            # Checking for the domain
            if kwargs["ENTERPRISE_ID"] == None:
                print("Enter the enterprise id before running the test.")
                time.sleep(2)
                sys.exit()
                
            # Creating a multiuser
            settings.TEMP_USR_CSV = create_users(kwargs["URL"][0], kwargs["NUM_USERS"], kwargs["ENTERPRISE_ID"])

            # Loading NLP server locust class
            from dave_ai_kiosk_testing.plans import nlp_server_multi_user
            if kwargs["WARMUP"]:
                print(created_heading("Running a warmup session.", "-"))
                run_component(user_classes=[nlp_server_multi_user.UserBehavior], **kwargs)
                print(created_heading("Warmup session is done. The load testing will start.", "-"))
                kwargs["WARMUP"] = False
                clean_repport_dir()

            # Running NLP component        
            run_component(user_classes=[nlp_server_multi_user.UserBehavior], **kwargs)

        else:
            
            # Loading NLP server locust class
            from dave_ai_kiosk_testing.plans import nlp_server

            # Running NLP component        
            run_component(user_classes=[nlp_server.UserBehavior], **kwargs)

    elif kwargs["COMPONENT"] == "WAKE_ASR_NLP":

        # Updating ASR URL
        settings.WAKE_URL = kwargs["URL"][0]
        settings.ASR_URL = kwargs["URL"][1]
        settings.ASR_NLP_SERVER = kwargs["URL"][2]

        # Loading audio data
        settings.AUDIO_COLLECTION = settings.AUDIO_DATA_PATH.joinpath(test_set_collection("audio", kwargs["DOMAIN"]))

        if (os.path.isfile(settings.AUDIO_COLLECTION)):
            print("Audio data is available for load testing.\n\n")
        else:
            from dave_ai_kiosk_testing.data.load_data import get_audio_data
            print("Audio data is not available. Will be downloaded from the server.\n\n")
            get_audio_data(kwargs["DOMAIN"])

        # Loading ASR server locust class
        from dave_ai_kiosk_testing.plans import wake_asr_nlp

        if kwargs["WARMUP"]:
            print(created_heading("Running a warmup session.", "-"))
            run_component(user_classes=[wake_asr_nlp.UserBehaviorWake], **kwargs)
            print(created_heading("Warmup session is done. The load testing will start.", "-"))
            kwargs["WARMUP"] = False
            clean_repport_dir()

        run_component(user_classes=[wake_asr_nlp.UserBehaviorWake], **kwargs)
      
    elif kwargs["COMPONENT"] == "ASR_NLP":

        # Updating ASR URL
        settings.ASR_URL = kwargs["URL"][0]
        settings.ASR_NLP_SERVER = kwargs["URL"][1]

        # Loading audio data
        settings.AUDIO_COLLECTION = settings.AUDIO_DATA_PATH.joinpath(test_set_collection("audio", kwargs["DOMAIN"]))
                
        if (os.path.isfile(settings.AUDIO_COLLECTION)):
            print("Audio data is available for load testing.\n\n")
        else:
            from dave_ai_kiosk_testing.data.load_data import get_audio_data
            print("Audio data is not available. Will be downloaded from the server.\n\n")
            get_audio_data(kwargs["DOMAIN"])

        # Loading ASR server locust class
        from dave_ai_kiosk_testing.plans import asr_nlp_server,asr_nlp_server_chunks

        if kwargs["WARMUP"]:
            print(created_heading("Running a warmup session.", "-"))
            if kwargs["CHUNKS"]:
                # Running ASR component with chunks
                run_component(user_classes=[asr_nlp_server_chunks.UserBehavior], **kwargs)
            else:
                # Running ASR component
                run_component(user_classes=[asr_nlp_server.UserBehavior], **kwargs)
            print(created_heading("Warmup session is done. The load testing will start.", "-"))
            kwargs["WARMUP"] = False
            clean_repport_dir()

        if kwargs["CHUNKS"]:
            # Running ASR component with chunks
            run_component(user_classes=[asr_nlp_server_chunks.UserBehavior], **kwargs)
        else:
            # Running ASR component
            run_component(user_classes=[asr_nlp_server.UserBehavior], **kwargs)
    
    elif kwargs["COMPONENT"] == "IMAGE":

        # Updating Image URL
        settings.IMAGE_PRO_URL = kwargs["URL"][0]

        # Loading image data
        settings.IMAGE_COLLECTION = settings.IMAGE_DATA_PATH.joinpath(test_set_collection("image", kwargs["DOMAIN"]))
        
        if (os.path.isfile(settings.IMAGE_COLLECTION)):
            print("Image data is available for load testing.\n\n")
        else:
            from dave_ai_kiosk_testing.data.load_data import get_image_data
            print("Image data is not available. Will be downloaded from the server.\n\n")
            get_image_data(kwargs["DOMAIN"])
        
        # Updating image classifier
        if kwargs["IMAGE_CLASS"] == None:
            print("\n\nEnter the image classifier before running the test.")
            time.sleep(2)
            sys.exit()
        else:
            settings.IMAGE_CLASSIFIER = kwargs["IMAGE_CLASS"]
            settings.IMAGE_RECG = kwargs["IMAGE_RECG"]

        # Loading ASR server locust class
        from dave_ai_kiosk_testing.plans import image_processing_server, image_processing_server_video
        
        # Running Image Processing component
        if kwargs["IMAGE_DATA_TYPE"] == "image":
            run_component(user_classes=[image_processing_server.UserBehavior], **kwargs)
        elif kwargs["IMAGE_DATA_TYPE"] == "video":
            run_component(user_classes=[image_processing_server_video.UserBehavior], **kwargs)
        else:
            print("\n\nMention the correct image data type for test. Example: 'image' or 'video'.")
            time.sleep(2)
            sys.exit()
    
    elif kwargs["COMPONENT"] == "WAKE_ASR_IMAGE":

        # Updating ASR URL
        settings.WAKE_URL = kwargs["URL"][0]
        settings.ASR_URL = kwargs["URL"][1]
        settings.IMAGE_PRO_URL = kwargs["URL"][2]

        # Loading audio data
        settings.AUDIO_COLLECTION = settings.AUDIO_DATA_PATH.joinpath(test_set_collection("audio", kwargs["DOMAIN"]))

        # Loading image data
        settings.IMAGE_COLLECTION = settings.IMAGE_DATA_PATH.joinpath(test_set_collection("image", "face_recg"))

        if (os.path.isfile(settings.AUDIO_COLLECTION)):
            print("Audio data is available for load testing.\n\n")
        else:
            from dave_ai_kiosk_testing.data.load_data import get_audio_data
            print("Audio data is not available. Will be downloaded from the server.\n\n")
            get_audio_data(kwargs["DOMAIN"])

        if (os.path.isfile(settings.IMAGE_COLLECTION)):
            print("Image data is available for load testing.\n\n")
        else:
            from dave_ai_kiosk_testing.data.load_data import get_image_data
            print("Image data is not available. Will be downloaded from the server.\n\n")
            get_image_data(kwargs["DOMAIN"])

        # Updating image classifier
        if kwargs["IMAGE_CLASS"] == None:
            print("\n\nEnter the image classifier before running the test.")
            time.sleep(2)
            sys.exit()
        else:
            settings.IMAGE_CLASSIFIER = kwargs["IMAGE_CLASS"]

        # Loading ASR server locust class
        from dave_ai_kiosk_testing.plans import wake_asr_image
        
        # Running ASR+WAKE+IMAGE component
        run_component(user_classes=[wake_asr_image.UserBehaviorWake, wake_asr_image.UserBehaviorFace], **kwargs)

    elif kwargs["COMPONENT"] == "WAKE_ASR_NLP_IMAGE":

        # Updating ASR URL
        settings.WAKE_URL = kwargs["URL"][0]
        settings.ASR_URL = kwargs["URL"][1]
        settings.ASR_NLP_SERVER = kwargs["URL"][2]
        settings.IMAGE_PRO_URL = kwargs["URL"][3]

        # Loading audio data
        settings.AUDIO_COLLECTION = settings.AUDIO_DATA_PATH.joinpath(test_set_collection("audio", kwargs["DOMAIN"]))

        # Loading image data
        settings.IMAGE_COLLECTION = settings.IMAGE_DATA_PATH.joinpath(test_set_collection("image", "face_recg"))

        if (os.path.isfile(settings.AUDIO_COLLECTION)):
            print("Audio data is available for load testing.\n\n")
        else:
            from dave_ai_kiosk_testing.data.load_data import get_audio_data
            print("Audio data is not available. Will be downloaded from the server.\n\n")
            get_audio_data(kwargs["DOMAIN"])

        if (os.path.isfile(settings.IMAGE_COLLECTION)):
            print("Image data is available for load testing.\n\n")
        else:
            from dave_ai_kiosk_testing.data.load_data import get_image_data
            print("Image data is not available. Will be downloaded from the server.\n\n")
            get_image_data(kwargs["DOMAIN"])

        # Updating image classifier
        if kwargs["IMAGE_CLASS"] == None:
            print("\n\nEnter the image classifier before running the test.")
            time.sleep(2)
            sys.exit()
        else:
            settings.IMAGE_CLASSIFIER = kwargs["IMAGE_CLASS"]

        # Loading ASR server locust class
        from dave_ai_kiosk_testing.plans import wake_asr_nlp_image
        
        # Running ASR+WAKE+IMAGE component
        run_component(user_classes=[wake_asr_nlp_image.UserBehaviorWake, wake_asr_nlp_image.UserBehaviorFace], **kwargs)

    elif kwargs["COMPONENT"] == "WAKE_ASR_NLP_IMAGE2":

        # Updating ASR URL
        settings.WAKE_URL = kwargs["URL"][0]
        settings.ASR_URL = kwargs["URL"][1]
        settings.ASR_NLP_SERVER = kwargs["URL"][2]
        settings.IMAGE_PRO_URL = kwargs["URL"][3]
        if len(kwargs["URL"]) == 4:
            settings.IMAGE_PRO_URL_2 = kwargs["URL"][3]
        else:
            settings.IMAGE_PRO_URL_2 = kwargs["URL"][4]

        # Loading audio data
        settings.AUDIO_COLLECTION = settings.AUDIO_DATA_PATH.joinpath(test_set_collection("audio", kwargs["DOMAIN"]))

        # Loading image data
        settings.IMAGE_COLLECTION = settings.IMAGE_DATA_PATH.joinpath(test_set_collection("image", "face_recg"))
        settings.IMAGE_COLLECTION2 = settings.IMAGE_DATA_PATH.joinpath(test_set_collection("image", "number%20plate"))

        if (os.path.isfile(settings.AUDIO_COLLECTION)):
            print("Audio data is available for load testing.\n\n")
        else:
            from dave_ai_kiosk_testing.data.load_data import get_audio_data
            print("Audio data is not available. Will be downloaded from the server.\n\n")
            get_audio_data(kwargs["DOMAIN"])

        if (os.path.isfile(settings.IMAGE_COLLECTION)):
            print("Image data is available for load testing.\n\n")
        else:
            from dave_ai_kiosk_testing.data.load_data import get_image_data
            print("Image data is not available. Will be downloaded from the server.\n\n")
            get_image_data(kwargs["DOMAIN"])

        # Updating image classifier
        if kwargs["IMAGE_CLASS"] == None:
            print("\n\nEnter the image classifier before running the test.")
            time.sleep(2)
            sys.exit()
        else:
            settings.IMAGE_CLASSIFIER = kwargs["IMAGE_CLASS"]

        # Loading ASR server locust class
        from dave_ai_kiosk_testing.plans import wake_asr_nlp_image2
        
        # Running ASR+WAKE+IMAGE component
        run_component(user_classes=[wake_asr_nlp_image2.UserBehaviorWake, wake_asr_nlp_image2.UserBehaviorFace, wake_asr_nlp_image2.UserBehaviorVehicle], **kwargs)

    elif kwargs["COMPONENT"] == "IMAGE2":

        # Updating ASR URL
        settings.IMAGE_PRO_URL = kwargs["URL"][0]
        if len(kwargs["URL"]) == 1:
            settings.IMAGE_PRO_URL_2 = kwargs["URL"][0]
        else:
            settings.IMAGE_PRO_URL_2 = kwargs["URL"][1]

        # Loading image data
        settings.IMAGE_COLLECTION = settings.IMAGE_DATA_PATH.joinpath(test_set_collection("image", "face_recg"))
        settings.IMAGE_COLLECTION2 = settings.IMAGE_DATA_PATH.joinpath(test_set_collection("image", "number%20plate"))

        settings.IMAGE_CLASSIFIER = kwargs["IMAGE_CLASS"]
        settings.IMAGE_RECG = kwargs["IMAGE_RECG"]

        # Defining waitime between each request
        settings.WAIT_TIME = constant(kwargs["WAIT"])

        # Loading ASR server locust class
        from dave_ai_kiosk_testing.plans import image_processing_server2
        
        # Running ASR+WAKE+IMAGE component
        run_component(user_classes=[image_processing_server2.UserBehaviorImageFace, image_processing_server2.UserBehaviorImageVehicle], **kwargs)
    
    elif kwargs["COMPONENT"] == "WAKE_ASR_IMAGE2":

        # Updating ASR URL
        settings.WAKE_URL = kwargs["URL"][0]
        settings.ASR_URL = kwargs["URL"][1]
        settings.IMAGE_PRO_URL = kwargs["URL"][2]

        # Loading audio data
        settings.AUDIO_COLLECTION = settings.AUDIO_DATA_PATH.joinpath(test_set_collection("audio", kwargs["DOMAIN"]))

        # Loading image data
        settings.IMAGE_COLLECTION = settings.IMAGE_DATA_PATH.joinpath(test_set_collection("image", "face_recg"))
        settings.IMAGE_COLLECTION2 = settings.IMAGE_DATA_PATH.joinpath(test_set_collection("image", "number%20plate"))

        if (os.path.isfile(settings.AUDIO_COLLECTION)):
            print("Audio data is available for load testing.\n\n")
        else:
            from dave_ai_kiosk_testing.data.load_data import get_audio_data
            print("Audio data is not available. Will be downloaded from the server.\n\n")
            get_audio_data(kwargs["DOMAIN"])

        if (os.path.isfile(settings.IMAGE_COLLECTION)):
            print("Image data is available for load testing.\n\n")
        else:
            from dave_ai_kiosk_testing.data.load_data import get_image_data
            print("Image data is not available. Will be downloaded from the server.\n\n")
            get_image_data(kwargs["DOMAIN"])

        # Updating image classifier
        if kwargs["IMAGE_CLASS"] == None:
            print("\n\nEnter the image classifier before running the test.")
            time.sleep(2)
            sys.exit()
        else:
            settings.IMAGE_CLASSIFIER = kwargs["IMAGE_CLASS"]

        # Defining waitime between each request
        settings.WAIT_TIME = constant(kwargs["WAIT"])
        
        # Loading ASR server locust class
        from dave_ai_kiosk_testing.plans import wake_asr_image2
        
        # Running ASR+WAKE+IMAGE component
        run_component(user_classes=[wake_asr_image2.UserBehaviorWake, 
                                    wake_asr_image2.UserBehaviorFace, 
                                    wake_asr_image2.UserBehaviorVehicle], **kwargs)

    elif kwargs["COMPONENT"] == "IDLE":

        # Running IDLE component for idle system utilization
        run_component(user_classes=[], **kwargs)

    else:

        print("Mentioned component is not available for testing. Please check for --help.")
        time.sleep(2)
        sys.exit()
        
