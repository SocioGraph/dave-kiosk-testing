"""
THE QUICK WAY TO CREATE NEW USERS IN THE DASHBOARD FOR TESTING.
USE THIS ONLY WHEN YOU ARE TESTING A LOCAL DEVICE.
"""

from argparse import ArgumentParser
from .utils import create_users

if __name__=="__main__":
    parser = ArgumentParser(description='Configuration to run bechamarking.',
                            add_help=False)

    parser.add_argument("--ENTERPRISE_ID", default=None, help="Mention component to start benchmarking.")
    parser.add_argument("--URL", default=None, help="Mention the target URL.")
    
    arguments = vars(parser.parse_args())
    create_users(nlp_url=arguments["URL"], num_users=1, enterprise_id=arguments["ENTERPRISE_ID"], stdout=True)          