import json
import time
from urllib.parse import uses_fragment
import gevent
import logging
import csv
import string
from collections import defaultdict
from datetime import datetime
from random import choice
from argparse import ArgumentParser, SUPPRESS


from locust.env import Environment
from locust.log import setup_logging
from locust import HttpUser, task
from locust.user.wait_time import constant
from locust_plugins.csvreader import CSVDictReader

from locust import stats
from locust.stats import StatsCSVFileWriter, stats_printer, stats_history
from locust.stats import print_error_report, print_percentile_stats, print_stats, stats_printer, stats_history

logger = logging.getLogger(__name__)

with open("config.json", "r") as cfg:
        config = json.load(cfg)

class UserBehavior(HttpUser):

    host = config["host_url"] + "/conversation/" + config["conversation_id"] + "/" + config["customer_id"]
    wait_time = constant(config["wait_time"])
    ssn_reader = CSVDictReader(config["conversation_filepath"])
    engagement_id = config["engagement_id"]

    @task
    def random_conversation(self):

        # Generating conversation text
        random_conv = next(UserBehavior.ssn_reader)

        # Creating a payload to start conversation 
        nlp_payload = {}
        nlp_payload["customer_response"] = random_conv["utterance"]
        nlp_payload["system_response"] = "sr_init"
        nlp_payload["refresh_cache"] = False
        if UserBehavior.engagement_id:
            nlp_payload["engagement_id"] = UserBehavior.engagement_id
        
        start_at = time.time()

        with self.client.post(UserBehavior.host, headers=UserBehavior.NLP_HEADER, data=json.dumps(nlp_payload), catch_response=True) as response:
            response_json = response.json()
            print("*Conversation*")
            print("Original Customer State: ",random_conv["customer_state"])
            print("\n")
            print("Predicted Customer state: ",response_json["customer_state"])
            print("\n\n")

            # Saving data exchange in a CSV file
            with open("request_data_exchange.csv", "a") as csv_file:
                w = csv.DictWriter(csv_file, ["original_cs", "predicted_cs", "response_start_time","response_got_time","response_time"])
                if csv_file.tell() == 0:
                    w.writeheader()
                w.writerow({
                    "original_cs": random_conv["customer_state"],
                    "predicted_cs": response_json["customer_state"],
                    "response_start_time":start_at,
                    "response_got_time": time.time(),
                    "response_time": (time.time()-start_at),
                })

def main(**kwargs):

    # Mention the name of the files prefix to display
    FILENAME_PREFIX =  "nlp_load_test"
    LOGGER_FILE = "gvent_logs.log"

    # Number of users and the spawn rate
    NO_OF_USERS = kwargs["NUM_USERS"]
    SPAWN_RATE = kwargs["SPAWN_RATE"]
    RUNNER_TIME = kwargs["RUN_TIME"]

    # Setup Environment and Runner
    env = Environment(user_classes=UserBehavior, catch_exceptions= True)
    runner = env.create_local_runner()

    # Logging all the stdout to a file.
    gevent.spawn(setup_logging("INFO", LOGGER_FILE))

    # Start a greenlet that periodically outputs the current stats
    gevent.spawn(stats_printer(runner.stats))
    gevent.spawn(print_stats(runner.stats))
    gevent.spawn(print_error_report(runner.stats))
    gevent.spawn(print_percentile_stats(runner.stats))

    # Start a greenlet that save current stats to history
    gevent.spawn(stats_history, runner)
    gevent.sleep(0)

    # Loading data to the CSV file.
    stats_csv_writer = StatsCSVFileWriter(
                environment = env, 
                percentiles_to_report = stats.PERCENTILES_TO_REPORT, 
                base_filepath = FILENAME_PREFIX, 
                full_history =  True
            )

    # Writing all the stats to a CSV file
    gevent.spawn(stats_csv_writer.stats_writer)

    # Starting the test runner
    runner.start(user_count=NO_OF_USERS, spawn_rate=SPAWN_RATE)

    # Stops the runner after RUNNER_TIME seconds
    gevent.spawn_later(RUNNER_TIME, lambda: runner.quit())

    runner.greenlet.join()


if __name__=="__main__":
    parser = ArgumentParser(description='Configuration to run bechamarking.',
                            add_help=False)

    required = parser.add_argument_group('required arguments')
    optional = parser.add_argument_group('optional arguments')

    # Adding required arguments
    required.add_argument("--URL", default=None, help="Mention the target URL(s).")
    #required.add_argument("--CONFIG", default=None, help="Running the testing framework using a config file.")
    #required.add_argument("--CONV", default=None, help="Conversation file.")

    # Adding optional arguments
    optional.add_argument("-h", "--help", action="help", default=SUPPRESS, help='show this help message and exit')
    optional.add_argument("--NUM_USERS", default=1, help="Peak number of concurrent Locust users. Default value: 1", type=int)
    optional.add_argument("--SPAWN_RATE", default=1, help="Rate to spawn users at (users per second). Default value: 1", type=int)
    optional.add_argument("--WAIT", default=1, help="Wait time between each user load. Default value: 1 seconds]", type=int)
    optional.add_argument("--RUN_TIME", default=60, help="Stop after the specified amount of time (in seconds). Default value: 60", type=int)
    
    # Parsing arguments
    arguments = vars(parser.parse_args())
    main(**arguments)