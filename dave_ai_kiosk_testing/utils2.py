import string
from random import choice
from pathlib import Path

def random_report_dir_creation(size=5):
    """
    Create a random directory to store reports.
    """
    new_dir = ''.join(choice(string.ascii_uppercase) for _ in range(size))
    Path("reports/"+new_dir).mkdir(parents=True, exist_ok=True)
    return new_dir

def test_set_collection(data, domain):
    """
    Getting audio collection file based on domain. 
    The function return file to be used for test sets.
    """
    filename = ""
    
    if domain == None:
        return data + "_collection.csv"
    else:
        return data + "_collection_" + domain + ".csv"