import subprocess
import sys
import time
import yaml
from argparse import ArgumentParser, SUPPRESS
from datetime import datetime
from .utils import component_sha, franmework_sha

if __name__=="__main__":
    parser = ArgumentParser(description='Configuration to run bechamarking.',
                            add_help=False)

    required = parser.add_argument_group('required arguments')
    optional = parser.add_argument_group('optional arguments')

    # Adding required arguments
    required.add_argument("--COMPONENT", default=None, help="Mention component to start benchmarking.")
    required.add_argument("--URL", default=None, help="Mention the target URL(s).", nargs='+')
    required.add_argument("--CONFIG", default=None, help="Running the testing framework using a config file.")

    # Adding optional arguments
    optional.add_argument("-h", "--help", action="help", default=SUPPRESS, help='show this help message and exit')
    optional.add_argument("--POOL_BACK", default=True, help="By default, the polling will run in the background.")
    optional.add_argument("--WARMUP", default=False, help="By default, system will not do the warmup. This can be ran for first time on a device.", type=bool)
    optional.add_argument("--MULTI_DEVICE", default=False, help="By default, only single deivce will be testing for benchmarking.", type=bool)
    optional.add_argument("--MULTI_USER", default=False, help="By default, default admin account will be used for bechmarking.", type=bool)
    optional.add_argument("--CONV_ID", default="deployment_kiosk", help="By default, deployment_kiosk will be used for bechmarking.")
    optional.add_argument("--NUM_USERS", default=1, help="Peak number of concurrent Locust users. Default value: 1", type=int)
    optional.add_argument("--SPAWN_RATE", default=1, help="Rate to spawn users at (users per second). Default value: 1", type=int)
    optional.add_argument("--WAIT", default=1, help="Wait time between each user load. Default value: 1 seconds]", type=int)
    optional.add_argument("--RUN_TIME", default=60, help="Stop after the specified amount of time (in seconds). Default value: 60", type=int)
    optional.add_argument("--DOMAIN", default=None, help="Selecting a domain for data to be loaded for load testing.")
    optional.add_argument("--ENTERPRISE_ID", default=None, help="Selecting a enterprise will be used for temp users for load testing.")
    optional.add_argument("--IMAGE_RECG", default="default", help="Selecting a recognizer for the image classifier. For default,'default' will be slected where general tensorflow model will be used. For 'openvino', openvino models will be used.`.")
    optional.add_argument("--IMAGE_CLASS", default=None, help="Selecting a classifier for data will select the image classifier for prediction.")
    optional.add_argument("--IMAGE_FRAME", default=0.3, help="Time (in milliseconds) for each frame sent to the server", type=float)
    optional.add_argument("--IMAGE_DATA_TYPE", default="image", help="Selecting a type of data used for the image classifier for prediction.")
    optional.add_argument("--SSL_VERIFY", default=True, help="By default, it will be True. User can change it when it is required.")
    optional.add_argument("--K_MODEL", default="", help="By default, a general kaldi model is be selected.")
    optional.add_argument("--RECG", default="kaldi", help="By default, kaldi is selected as a recognizer.")
    optional.add_argument("--CHUNKS", default=False, help="The audio data will be sent in chunks if it is true.")
    optional.add_argument("--CHUNK_SIZE", default=1000, help="By default, 1000 milliseconds is selected. The values should be in milliseconds.", type=int)
    optional.add_argument("--HARDWARE", default=[""], help="During test, mention the name and the simple configuration of the device.", nargs='+')
    arguments = vars(parser.parse_args())
    
    # Adding additional informations before load testing
    arguments["framework_sha"] = franmework_sha()
    arguments["component_sha"] = component_sha(**arguments)
    arguments["test_run_time"] = datetime.now().strftime("%m/%d/%Y, %H:%M:%S")

    # Running main component
    from .main import main

    if arguments["CONFIG"] == None:
        if arguments["COMPONENT"] == None or arguments["URL"] == None:
            print("\nMandatory arguments are not mentioned.")
            time.sleep(1)
            sys.exit(0)
        else:
            main(**arguments)
            #print(arguments)
    else:
        with open(arguments["CONFIG"], "r") as yml:
            configs = yaml.safe_load(yml)

        if set(arguments.keys()) <= configs.keys():
            print("\nConfig file is incomplete.")
            time.sleep(1)
            sys.exit(0)
        #main(**configs)
        print(configs)