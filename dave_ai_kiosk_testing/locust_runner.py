import gevent
import sys
import time
import logging
from locust.env import Environment
from locust.log import setup_logging
from locust import stats
from locust.stats import StatsCSVFileWriter, stats_printer, stats_history
from locust.stats import print_error_report, print_percentile_stats, print_stats, stats_printer, stats_history

# Importing constants from settings.py
from .settings import REPORTS_DIR

# Updating parameter in analysis
from . import analysis

# Mention the name of the files prefix to display
FILENAME_PREFIX =  "test"

REPORTS_PATH = "reports/" + REPORTS_DIR

# CSV File reports path
CSV_FILEPATH = REPORTS_PATH + '/' + FILENAME_PREFIX

logger = logging.getLogger(__name__)

def quit_runner(runner):
    time.sleep(30)
    runner.quit()

def locust_local_runner(user_classes, *args, **kwargs):

    # Number of users and the spawn rate
    SPAWN_RATE = kwargs["SPAWN_RATE"]

    # Time to stop runner (in secconds)
    if kwargs["WARMUP"] == True:
        RUNNER_TIME = kwargs["MODIFIED_RUN_TIME"]
    else:
        RUNNER_TIME = kwargs["RUN_TIME"]

    # Setup Environment and Runner

    env = Environment(user_classes=user_classes, catch_exceptions= True)
    runner = env.create_local_runner()

    # Logging all the stdout to a file.
    LOGGER_FILE = "reports/"+REPORTS_DIR + "/logs.log"
    gevent.spawn(setup_logging("INFO", LOGGER_FILE))

    # Start a greenlet that periodically outputs the current stats
    gevent.spawn(stats_printer(runner.stats))
    gevent.spawn(print_stats(runner.stats))
    gevent.spawn(print_error_report(runner.stats))
    gevent.spawn(print_percentile_stats(runner.stats))

    # Start a greenlet that save current stats to history
    gevent.spawn(stats_history, runner)
    gevent.sleep(0)

    # Loading data to the CSV file.
    stats_csv_writer = StatsCSVFileWriter(
                environment = env, 
                percentiles_to_report = stats.PERCENTILES_TO_REPORT, 
                base_filepath = CSV_FILEPATH, 
                full_history =  True
            )

    # Writing all the stats to a CSV file
    gevent.spawn(stats_csv_writer.stats_writer)

    # Modifying the num_users
    if len(user_classes) > 1 and kwargs["NUM_USERS"] == 1:
        NO_OF_USERS = len(user_classes) * kwargs["NUM_USERS"]
        analysis.MODIFIED_NUM_USERS = NO_OF_USERS
        analysis.NUM_OF_COMPONENTS = len(user_classes)
    else:
        NO_OF_USERS = kwargs["NUM_USERS"]

    # Starting the test runner
    runner.start(user_count=NO_OF_USERS, spawn_rate=SPAWN_RATE)

    # Stops the runner after RUNNER_TIME seconds
    gevent.spawn_later(RUNNER_TIME, lambda: runner.quit())

    runner.greenlet.join()