import time
from .locust_runner import locust_local_runner

from .utils import start_system_stat, stop_system_stats
from .analysis import all_statistics
from .settings import REPORTS_DIR
from .config import HOST_URL, HOST_URL_EDGE


def run_component(user_classes, *args, **kwargs):

    print("Started load testing and sending signal to host server to collect resource uttilisation data.\n")
    
    if kwargs["MULTI_DEVICE"] == True:
        resource_uid_1 = start_system_stat(HOST_URL)
        resource_uid_2 = start_system_stat(HOST_URL_EDGE)
        locust_local_runner(user_classes=user_classes, **kwargs)
        stop_system_stats(HOST_URL,uid=resource_uid_1)
        stop_system_stats(HOST_URL_EDGE, uid=resource_uid_2, flag="_edge")
        time.sleep(10)
        if kwargs["WARMUP"] == False:
            all_statistics(**kwargs)
    elif kwargs["COMPONENT"] == "IDLE":
        resource_uid_1 = start_system_stat(HOST_URL)
        time.sleep(kwargs["RUN_TIME"])
        stop_system_stats(HOST_URL,uid=resource_uid_1)
        time.sleep(10)
        all_statistics(**kwargs)
    else:
        resource_uid = start_system_stat(HOST_URL)
        locust_local_runner(user_classes=user_classes, **kwargs)
        stop_system_stats(HOST_URL, uid=resource_uid)
        time.sleep(10)
        if kwargs["WARMUP"] == False:
            all_statistics(**kwargs)
    
    if kwargs["WARMUP"] == False:
        print("Load testing completed. The reports can be found in the 'reports/"+ REPORTS_DIR +"' directory.\n")