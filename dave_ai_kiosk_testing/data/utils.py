import base64
import csv
import string
import requests
import subprocess
from base64 import b64encode
from random import randint, random


def random_recognition_sid(): 
    return "".join( [random.choice(string.digits) for i in range(8)] )

def save_audio_file(url, file_name):
    response = requests.get(url)  
    with open("audio_data/mp3/" + file_name + ".mp3", 'wb') as f:
        f.write(response.content)

def mp3_wav(mp3_file_path):
    """
    Converting mp3 audio file to wav format
    """
    subprocess.call(['ffmpeg', '-i', 'audio.mp3','audio.wav'])

def wav_base64(wav_file):
    """
    Encoding wav audio file to base64 encoding.
    """
    return str(b64encode(wav_file))