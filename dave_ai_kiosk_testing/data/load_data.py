import json
import time
import sys
from pathlib import Path
import subprocess
import csv
import string
from requests import request
from random import choice
from pydub import AudioSegment
from dave_ai_kiosk_testing.settings import IMAGE_COLLECTION, WAV_AUDIO_PATH, MP3_AUDIO_PATH, AUDIO_COLLECTION, IMAGE_DATA_PATH, DATA_PATH, AUDIO_DATA_PATH
from dave_ai_kiosk_testing.config import AUDIO_HEADERS, AUDIO_URL, IMAGE_URL, IMAGE_HEADERS

ASR_DOMAINS = ["general", "karnataka_bank", "restaurant", "wake_up", "qsr_restaurant", "kbl_asr_nlp", "maruti"]

ASR_DOMAINS_ZIP = ["kbl_clean", "kbl_clean_india", "kbl_clean_us", "qsr_clean_us", "wake_up_custom", "wake_up_kbl_custom", "qsr_clean_india", "msil_clean_india", "msil_clearn_us" "msil_recorded", "wake_up_dheera"]

IMAGE_DOMAINS = ["number%20plate", "person", "car", "person_test"]

#AUDIO_URL = AUDIO_URL+"&tags="+ASR_DOMAIN

# Audio data will downloaded as both mp3 and wav file. Wav file will be used for testing
CSV_HEADERS = ["file_name","utterance","environment","noise_level","language","duration"]

CSV_HEADERS_2 = ["file_name","utterance","environment","noise_level","language","duration","key"]

# Image data header
IMG_CSV_HEADERS = ["file_name", "label"]


def get_audio_data(domain=None):
    """
    To download audio data for ASR server load testing.
    """
    
    if AUDIO_URL == "" or AUDIO_HEADERS == {}:
        print("Update the server credential is config.py.\n\n")
        time.sleep(2)
        sys.exit()

    # Creating directories to download audio files
    MP3_AUDIO_PATH.mkdir(parents=True, exist_ok=True)
    WAV_AUDIO_PATH.mkdir(parents=True, exist_ok=True)

    if domain in ASR_DOMAINS_ZIP:
        print("The data will be extracted to audio data directory.")
        zip_audio = DATA_PATH.joinpath("zip_data").joinpath(domain+".zip")
        subprocess.call(["unzip", zip_audio, "-d", AUDIO_DATA_PATH])
        print("Completed extracting audio data.")
        return None

    if domain:
        if domain not in ASR_DOMAINS:
            print("Mention correct domain.\n\n")
            time.sleep(2)
            sys.exit()
        audio_url = AUDIO_URL + "&tags=" + domain
    else:
        audio_url = AUDIO_URL
    
    for page in range(1,11):
        # Getting a JSON response
        response = request("GET", audio_url+"&page_number="+str(page), headers=AUDIO_HEADERS, data={})
        response_json = response.json()
        #print(response_json)
        
        all_file_names = []

        if response_json["is_last"] == True:
            break
        
        for data in response_json["data"]:

            # Saving filename using utterance_id
            file_name = data["utterance_id"][:round(len(data["utterance_id"])/2)] + "".join([choice(string.digits) for i in range(5)])

            if file_name in all_file_names:
                continue

            # Downloading MP3 audio.
            try:
                mp3_response = request("GET", data["audio_url"])
                with open(MP3_AUDIO_PATH.joinpath(file_name +".mp3") , "wb") as mp3:
                    mp3.write(mp3_response.content)
            except:
                continue
            all_file_names.append(file_name)

            # Converting MP3 to WAV
            subprocess.call(["ffmpeg", "-i", MP3_AUDIO_PATH.joinpath(file_name +".mp3"), "-acodec","pcm_s16le","-ac","1", "-ar", "16000", WAV_AUDIO_PATH.joinpath(file_name +".wav"), "-y"])
            
            try:
                wav_audio_data = AudioSegment.from_wav(WAV_AUDIO_PATH.joinpath(file_name +".wav"))
            except FileNotFoundError:
                print("Error in FFMPEG conversion..")
                continue
            
            duration = wav_audio_data.duration_seconds
            

            with open(AUDIO_COLLECTION, "a") as f:
                if domain in ["qsr_restaurant", "kbl_asr_nlp"]:
                    w = csv.DictWriter(f, CSV_HEADERS_2)
                    if f.tell() == 0:
                        w.writeheader()
                    
                    w.writerow({
                        "file_name":file_name,
                        "utterance":data["utterance"],
                        "environment":data["environment"],
                        "noise_level":data["noise_level"],
                        "key":data["key"],
                        "language":data["language"],
                        "duration":duration
                    })
                else:
                    w = csv.DictWriter(f, CSV_HEADERS)
                    if f.tell() == 0:
                        w.writeheader()
                    
                    w.writerow({
                        "file_name":file_name,
                        "utterance":data["utterance"],
                        "environment":data["environment"],
                        "noise_level":data["noise_level"],
                        "language":data["language"],
                        "duration":duration
                    })
    print("Completed downloading audio data")

def get_image_data(domain=None):
    """
    To download audio data for ASR server load testing.
    """
    if domain:
        if domain not in IMAGE_DOMAINS:
            print("Mention correct domain.\n\n")
            time.sleep(2)
            sys.exit()
        image_url = IMAGE_URL + "?label_tags=" + domain
    else:
        image_url = IMAGE_URL
    
    print("**",image_url)
    print("**",IMAGE_COLLECTION)
    if IMAGE_URL == "" or IMAGE_HEADERS == {}:
        print("Update the server credential is config.py.\n\n")
        time.sleep(2)
        sys.exit()

    for page in range(1,7):
        # Getting a JSON response
        response = request("GET", image_url+"&page_number="+str(page), headers=IMAGE_HEADERS, data={})
        response_json = response.json()

        print("**",response.status_code)
        if response_json["is_last"] == True:
            break

        for data in response_json["data"]:

            # Saving filename using utterance_id
            file_name = data["image_id"]

            # Downloading the image
            try:
                img_response = request("GET", data["image_url"])
                with open(IMAGE_DATA_PATH.joinpath(file_name +".jpg") , "wb") as img:
                    img.write(img_response.content)
            except:
                continue

            with open(IMAGE_COLLECTION, "a") as f:
                w = csv.DictWriter(f, IMG_CSV_HEADERS)
                if f.tell() == 0:
                    w.writeheader()
                
                if domain == "person_test":
                    label = "Face" if "face" in data["label_tags"] else "No Face"
                else:
                    label = ""

                w.writerow({
                    "file_name": file_name,
                    "label": label
                })
    print("Completed downloading image data")