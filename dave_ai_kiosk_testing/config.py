"""
Make sure this file is updated before running the framework
"""

# Wake-up and ASR Ratio
WAKE_ASR_RATIO = [1,2]


# Update the below variables to fetch the audio data
AUDIO_URL = ""
AUDIO_HEADERS = {}

# Update the below variables to fetch the audio data
IMAGE_URL = ""
IMAGE_HEADERS = {}


# Update the below variable to fetch resource utilisation of the host system.
HOST_URL = ""
HOST_URL_EDGE = ""

# NLP Singup Keys for Account Creation
NLP_SIGNUP_API = {
  "dave_restaurant": "ZGF2ZSBleHBvMTU5NzEyNzc0NyA1Ng__",
  "karnataka_bank": "ZGF2ZSBleHBvMTU5NzEyNzc0NyA1Ng__"
}
NLP_CONV_ID = "deployment_kiosk"

# Update below credential to run send and recieve data for NLP serverdd
NLP_CONV_ID = ""
NLP_CUST_ID = ""
NLP_ENG_ID = ""
NLP_HEADER = {}

# Update below credential to run NLP through ASR
ASR_NLP_HEADER = {}