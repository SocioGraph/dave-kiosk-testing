"""
THE QUICK WAY TO CREATE NEW USERS IN THE DASHBOARD FOR TESTING.
USE THIS ONLY WHEN YOU ARE TESTING A LOCAL DEVICE.
"""
import os
from dave_ai_kiosk_testing.data.load_data import ASR_DOMAINS, ASR_DOMAINS_ZIP, IMAGE_DOMAINS
from dave_ai_kiosk_testing.utils import created_heading, list_numbering

if __name__=="__main__":
    print(created_heading("Domains to load for ASR Benchmarking"))
    print("The below data will be download from DaveAI Audio repository.")
    list_numbering(ASR_DOMAINS)

    print(created_heading("Domains to load for ASR Benchmarking"))
    print("The below data will be download from the ZIP in the repository.")
    list_numbering(ASR_DOMAINS_ZIP)

    print(created_heading("Domains to load for Image Processing Benchmarking"))
    print("The below data will be download from DaveAI Image repository.")
    list_numbering(IMAGE_DOMAINS)
    
    print(created_heading("Domains to load for NLP Benchmarking"))
    print("The below data will be download CSV file in the repository.")
    nlp_csv_files = os.listdir("./dave_ai_kiosk_testing/data/text_data/")
    list_numbering(nlp_csv_files, trm=(16,4))
              