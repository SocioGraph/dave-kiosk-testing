import os
import math
import time
import socketio
import time
import string
import random
import numpy as np
from base64 import b64encode
import skvideo.datasets
import skvideo.io
from skimage.io import imsave

global ORIGINAL
ORIGINAL = 10*["Face"]+23*["No Face"]+26*["Face"]
ORIGINAL_P_FRAME = []

global OUTPUTS
OUTPUTS = []

VIDEO_FRAME_RATE = 25
SUB_SAMPLING_RATE = 1
SUB_SAMPLE = math.floor(VIDEO_FRAME_RATE/SUB_SAMPLING_RATE)

video_filename = "pexels-cottonbro-8090418.mp4"

video_media_info = skvideo.io.ffprobe(video_filename)
print(video_media_info)

videogen = skvideo.io.vreader(video_filename)

IMAGE_FRAMES = []
for i, frame in enumerate(videogen):
    i = i+1
    #print(i)
    if i % 25 == 0:
        IMAGE_FRAMES.append(frame)

print(len(ORIGINAL))
print(len(IMAGE_FRAMES))


# Instantiation a client
sio = socketio.Client(logger=False, engineio_logger=False)

# URL to connect
ws_url = "http://localhost:5050/img"

uid_conn = ''.join(random.choice(string.ascii_lowercase) for _ in range(5))

# Client connecting to a URL..
sio.connect(ws_url + "?uid=" +uid_conn, transports="websocket")
print("SID: ", sio.sid)

# Getting connection acknowledgement
@sio.event
def message(data):
    print(data)

# Getting intermediate of resutls
@sio.on("intermediateClassifierResults")
def intermediateClassifierResults(data):
    print("Result: ",data)
    OUTPUTS.append(str(data["classifier_result"][0]["Result"]))

def background_task(uid, recognition_sid):
    for _ in range(500):
        time.sleep(0.01)
        sio.emit('intermediateClassifierResults', data={"uid":uid,"sid":recognition_sid})

def get_image_data(img_array, recognition_sid, flag=True, recog_class="FaceDetectorCafe"):
    """
    Function create an image object to pass the values to Image Processing server.

    If flag=False, a stopping image data will be creatted with an emplty data and 'is_recording' is False.
    By default, flag=True.
    """
    temp = {}
    temp["sid"] = recognition_sid
    temp["is_recording"] = True

    if flag==False:
        temp["data"] = ""
        temp["is_recording"] = False
        return temp
    
    # saving temp image
    imsave("temp.png", img_array)

    temp["data"] = '[enterprise_id][conversation_id][customer_id][engagement_id][api_key][system_response][timestamp]||||{"recognizer_type":"selectedRecognizer", "classNames":["'
    temp["data"] = temp["data"] + recog_class
    temp["data"] = temp["data"] + '"]}||||data:image/jpeg;base64,'
    with open("temp.png", "rb") as frame_img:
        temp["data"] = temp["data"] + b64encode(frame_img.read()).decode("utf-8")
    temp["data"] = temp["data"] + '||||'

    os.remove("temp.png")
    return temp



#
recognition_sid = "".join([random.choice(string.digits) for i in range(8)])
#i=0
for frame in IMAGE_FRAMES:
    image_data = get_image_data(frame, recognition_sid, recog_class="FaceDetectorCafe")
    sio.start_background_task(background_task, uid_conn, recognition_sid)
    sio.emit('imageStream', data=image_data)

sio.sleep(10)

# Calculating accuracy

print("\nORIGINAL: ", ORIGINAL, len(ORIGINAL))
print("\nOUTPUTS: ", OUTPUTS, len(OUTPUTS))

correct_prediction = np.sum(np.equal(np.array(ORIGINAL, dtype=object),np.array(OUTPUTS, dtype=object)))
accuracy = round(correct_prediction/len(np.array(ORIGINAL)),4)

print("\nCorrect Prediction: ", str(correct_prediction))
print("\nAccuracy: ", str(accuracy))



sio.disconnect()

