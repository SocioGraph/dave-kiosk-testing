from pydub import AudioSegment
from pydub.utils import make_chunks
import math
import pathlib

# Creating a chunks of the audio data
def get_chunks(self, wav_file, recognition_sid):

    CHUNK_SIZE = 1000
    
    audio = AudioSegment.from_wav(wav_file)
    duration = math.ceil(audio.duration_seconds)
    chunks = list(range(0,duration*1000, CHUNK_SIZE))

    temp_chunk_path = pathlib.Path("temp_chunk.wav")
    audio_chunks = []

    for i in chunks:
        audio[i:i+CHUNK_SIZE].export(temp_chunk_path, format="wav")
        audio_chunks.append(self.get_audio_data(temp_chunk_path, recognition_sid=recognition_sid))
        pathlib.Path.unlink(temp_chunk_path)
    return audio_chunks