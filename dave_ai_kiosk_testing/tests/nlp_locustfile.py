import time
from locust import HttpUser, task, between

class QuickstartUser(HttpUser):
    wait_time = between(1, 5)

    @task
    def simple_text(self):
        self.client.get("/tags?q=i%20am%20dinesh")