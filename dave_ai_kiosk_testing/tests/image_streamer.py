import socketio
import time
import pandas as pd
from random import randint, choice
import string
from requests import request
from datetime import datetime
from base64 import b64encode, encodebytes

import logging



def makeuid(size=5):
    return ''.join(choice(string.ascii_lowercase) for _ in range(size))

recogn_sid =  "".join([choice(string.ascii_letters) for i in range(8)])

# Get audio data
def get_image_data(img_file, flag=True):
    temp = {}
    temp["sid"] = recogn_sid

    if flag==False:
        temp["data"] = ""
        temp["is_recording"] = False
        temp["size"] = 0
        return temp
    
    temp["data"] = '[enterprise_id][conversation_id][customer_id][engagement_id][api_key][system_response][timestamp]||||{"recognizer_type":"selectedRecognizer", "classNames":["LicenseNumberDetector"]}||||data:image/jpeg;base64,'
    
    with open(img_file, 'rb') as f:
        temp["data"] = temp["data"] + b64encode(f.read()).decode("utf-8")

    temp["data"] = temp["data"] + '||||'

    temp["is_recording"] = True
    return temp

try:

    # Instantiation a client
    sio = socketio.Client(logger=False, engineio_logger=False)

    # URL to connect
    ws_url = "http://localhost:5050/img"

    uid_conn = makeuid()

    # Client connecting to a URL..
    sio.connect(ws_url + "?uid=" +uid_conn, transports="websocket")
    print("My sid: ", sio.sid)

    # Getting acknowledgement of message
    @sio.event
    def message(data):
        print(data)

    iamge_data = get_image_data("NumberPlate_Swift.jpg")
    iamge_data["uid"] = uid_conn

    iamge_data_stop = get_image_data("", flag=False)
    iamge_data_stop["uid"] = uid_conn
    #print(iamge_data)


    print("Waiting for the response from intermediateClassifierResults")
    @sio.on('intermediateClassifierResults')
    def custom_event(data):
        print("**Got intermediateResults**",data, (time.time()), "\n\n")

    print("Emitting the image_data")
    sio.emit('imageStream', data=iamge_data)
    sio.emit('imageStream', data=iamge_data_stop)




    for i in range(10):
        time.sleep(5)
        print("Pooling for the response")
        sio.emit('intermediateClassifierResults', data={"uid":uid_conn,"sid":recogn_sid})
        time.sleep(2)
        sio.emit('intermediateClassifierResults', data={"uid":uid_conn,"sid":recogn_sid})
        time.sleep(2)
        sio.emit('intermediateClassifierResults', data={"uid":uid_conn,"sid":recogn_sid})
        time.sleep(5)
except KeyboardInterrupt as e:
        print("Stopping the process")