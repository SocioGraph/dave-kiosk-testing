import time
import sys
import json
import csv
import subprocess
import requests
from random import choice, randint
from requests import request
from pathlib import Path
from faker import Faker

from .settings import BASE_DIR, REPORT_PATH, TEMP_DIR
from .config import HOST_URL, NLP_SIGNUP_API


def clean_repport_dir():
    for file in REPORT_PATH.iterdir():
        if file.suffix in [".log", ".csv"]:
            file.unlink(missing_ok=True)


def create_users(nlp_url: str, num_users: int, enterprise_id: str, stdout=False):
    """
    
    """
    temp_csv_filename = str(int(time.time())) + ".csv"

    faker = Faker()

    try:
        headers = {
            "X-I2CE-SIGNUP-API-KEY":NLP_SIGNUP_API[enterprise_id],
            "X-I2CE-ENTERPRISE-ID":enterprise_id,
            "Content-Type": "application/json"
        }
    except:
        print("********** Incorrect Enterprise ID ***********")
        sys.exit()

    updated_user_id = {
        "karnataka_bank": "user_id",
        "dave_restaurant": "customer_id"
    }
    for user in range(num_users):

        user_name = faker.name()

        sign_up_body = {
            "validated": True,
            "person_type": "kiosk",
            "person_name": user_name,
            "email": user_name.lower().replace(" ", "_") + str(randint(111, 999)) + "@i2ce.in",
            "device_id" : "device1"
        }

        response = request("POST", nlp_url+"/customer-signup", headers=headers, data=json.dumps(sign_up_body))
        response_json = response.json()

        print(f"Created a temporary user {user_name} for {enterprise_id}\n")

        if stdout == True:
            print(f"user_id: {response_json[updated_user_id[enterprise_id]]}")
            print(f"api_key: {response_json['api_key']}")
            print(f"enterprise_id: {enterprise_id}")
        else:
            print(f"user_id: {response_json[updated_user_id[enterprise_id]]}")
            print(f"api_key: {response_json['api_key']}")
            print(f"enterprise_id: {enterprise_id}")

            with open(TEMP_DIR.joinpath(temp_csv_filename), "a") as csv_file:
                w = csv.DictWriter(csv_file, ["api_key", "user_id", "enterprise_id"])
                if csv_file.tell() == 0:
                    w.writeheader()
                w.writerow({
                    "api_key": response_json["api_key"],
                    "user_id": response_json[updated_user_id[enterprise_id]],
                    "enterprise_id": enterprise_id
                })

    print(f"\nSuccefully created {num_users} users for the load testing.")
    
    return temp_csv_filename

def created_heading(head_text, symb="*"):
    """
    To display heading with stars on the stdout.
    """
    point = 2*symb
    border = symb*(len(head_text)+6)
    heading = "\n" + point + " " + str(head_text) + " " + point +"\n"
    return border+heading+border

def list_numbering(lst, trm=None):
    for i, j in enumerate(lst):
        if trm:
            print(f"{i+1}. {j[trm[0]:(len(j)-trm[1])]}")
        else:
            print(f"{i+1}. {j}")

def start_system_stat(url, **kwargs):
    """
    Sending http request for host server to start collecting the system resource utilization.
    """
    try:
        response = request("GET", url+"/stats-record?record=start", timeout=5)
        response_json = response.json()
        return response_json["task"]["uid"]
    except:
        print("There seems to be an error in the HOST_URL. The task will be continued without resource utilisation collection.\n\n")
        time.sleep(2)
        return "NO-HOST-SERVER"

def stop_system_stats(url,uid, flag=""):
    """
    Sending http request for host server to stop collecting the system resource utilization.
    """
    
    if uid == "NO-HOST-SERVER":
        print("System resource utilisation collection not happened.\n\n")
    else:
        response = request("GET", url+"/stats-record?record=stop&recordid="+uid, timeout=5)
        response_json = response.json()
        
        # Downloading resource utilisation to a CSV file.
        time.sleep(5)
        csv = request("GET",url+response_json["task"]["csv_file"])
        csv_content = csv.content
        csv_file = open(REPORT_PATH.joinpath("resource_utils"+flag+".csv"), 'wb')
        csv_file.write(csv_content)
        csv_file.close()

def component_sha(**kwargs):
    """
    Get the version of the component.
    """
    comp_sha = []
    if kwargs["COMPONENT"] == "ASR" or kwargs["COMPONENT"] == "ASR_NLP":
        try:
            with requests.get(kwargs["URL"][0]+"/statusz") as response:
                r = response.json()
                comp_sha.append(("ASR", r["version"]))
        except Exception as err:
            print("Error accessing the component version for {}:{}".format("ASR", err))
    if kwargs["COMPONENT"] == "NLP":
        try:
            with requests.get(kwargs["URL"][0]+"/statusz") as response:
                r = response.json()
                comp_sha.append(("NLP", r["dashboard"]))
        except Exception as err:
            print("Error accessing the component version for {}:{}".format("NLP", err))
    if kwargs["COMPONENT"] == "ASR_NLP":
        try:
            with requests.get(kwargs["URL"][1]+"/statusz") as response:
                r = response.json()
                comp_sha.append(("NLP", r["dashboard"]))
        except Exception as err:
            print("Error accessing the component version for {}:{}".format("NLP", err))
    return comp_sha

def franmework_sha():
    """
    Get the framework version
    """
    git_sha = subprocess.Popen('git log -1 --pretty=format:"%H:%aI"', stdout=subprocess.PIPE, shell=True)
    return git_sha.communicate()[0].decode("utf-8")