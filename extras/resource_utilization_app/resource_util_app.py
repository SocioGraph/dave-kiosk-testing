"""
Run this application in the host system, where DAVE.AI components are running on docker containers.

Make sure this application is running before load testing. 
"""

from flask import Flask, request, jsonify
import os, psutil, multiprocessing, string, random, time,csv
from pathlib import Path
import argparse
from statistics import mean

RUN_IN_DEBUG_MODE = os.environ.get('RUN_IN_DEBUG_MODE', False)

app=Flask(__name__)

app.secret_key="veryV#3rySc67eTkey"

global taskQueue

def getUid(noOfCharecters=6):
    chars = string.ascii_letters + string.digits
    uid = ''.join(random.choice(chars) for n in range(noOfCharecters))
    return uid


def getStats():

    stats = {}

    stats["start_timestamp"] = time.time()
    # gives a single float value
    stats["cpu_percent_percpu"] = psutil.cpu_percent(percpu=True)
    stats["cpu_percent"] = mean(stats["cpu_percent_percpu"])
    
    # gives an object with many fields
    ram_info = psutil.virtual_memory()

    stats["ram_total_bytes"] = ram_info.used

    # IO part
    stats["io_read_bytes"] = psutil.disk_io_counters().read_bytes
    stats["io_write_bytes"] = psutil.disk_io_counters().write_bytes
    
    # you can convert that object to a dictionary 
    #dict(psutil.virtual_memory()._asdict())
    
    # you can have the percentage of used RAM
    stats["ram_percent"] = ram_info.percent
    
    # you can calculate percentage of available memory
    #psutil.virtual_memory().available * 100 / psutil.virtual_memory().total
    stats["timestamp"] = time.time()

    return stats

@app.route('/stats', methods = ['GET'])
def stats():
    out = {}
    print(request.args)
    if not request.args.get("recordid"):
        out["error"] = "recordid not given"
        out["current-stats"] = getStats()
    else:
        out = getStats()

    return jsonify(out)

@app.route('/stats-record', methods = ['GET'])
def recordStats():
    out = {}
    if request.args.get("record") == "start":
        task = {}
        task["record"] = "start"
        task["interval"] = request.args.get("interval", 1)
        task["uid"] = getUid()
        task["csv_file"] = f"/static/stats/stats_{task['uid']}.csv"
        taskQueue.put(task)
        out["task"] = task
        out["recordStatus"] = "placed"
    elif request.args.get("record") == "stop":
        task = {}
        task["record"] = "stop"
        task["uid"] = request.args.get("recordid")
        task["csv_file"] = f"/static/stats/stats_{task['uid']}.csv"
        taskQueue.put(task)
        out["task"] = task
        out["recordStatus"] = "stop request placed"

    return jsonify(out)

def queueMonitor():

    start_timestamp = None
    currentTaskId = None
    currentTaskInterval = 1
    stopTask = True
    statsList = []
    stats = {}

    def getStats_sleep(currentTaskInterval):
        print(f"Recording..... Interval {currentTaskInterval}")
        stats = getStats()
        print(stats)
        statsList.append(stats)
        time.sleep(currentTaskInterval)

    def generateCSV(products_list, recordid):
        print("Writing to CSV")
        keys = products_list[0].keys()
        print(keys)
        with open(f'./static/stats/stats_{recordid}.csv', 'w', newline='')  as output_file:
            dict_writer = csv.DictWriter(output_file, keys)
            dict_writer.writeheader()
            dict_writer.writerows(products_list)
    

    while True:
        if not taskQueue.empty():
            print("Found a task.")
            try:
                task = taskQueue.get()
                print(stopTask)
                if task["record"] == "start":
                    if not currentTaskId:
                        print("Starting Recording...")
                        stopTask = False
                        currentTaskInterval = int(task["interval"])
                        currentTaskId =  task["uid"]
                        getStats_sleep(currentTaskInterval)
                    else:
                        print("Process busy, task cannot be placed.")
                        task["error"] = "Process busy, task cannot be placed."
                elif task["record"] == "stop":
                    print("Stop task received.")
                    if task["uid"] == currentTaskId:
                        print(f"stopping current recording.. {currentTaskId}")
                        stopTask = True
                        print(statsList)
                        generateCSV(statsList, currentTaskId)
                        currentTaskId=None
                        stats = {}
                        statsList = []
                        
            except Exception as e:
                pass
               
        elif not stopTask:
            getStats_sleep(currentTaskInterval)

        else:
            time.sleep(0.1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--PORT', help = "port to run", default = 5005)
    args = parser.parse_args()
    manager = multiprocessing.Manager()
    taskQueue = manager.Queue()

    queueMonitor = multiprocessing.Process(name = "queueMonitor", target = queueMonitor)

    print("Starting queue monitor.")
    queueMonitor.start()
    Path("static/stats").mkdir(parents=True, exist_ok=True)
    app.run(debug=True, host='0.0.0.0', port=args.PORT)
