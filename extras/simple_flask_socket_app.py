from flask import Flask, render_template
from flask_socketio import SocketIO, send, emit
from engineio.payload import Payload

# Creating app name
app = Flask(__name__)

# Since all the connection, SECRET_KEY is required
app.config['SECRET_KEY'] = 'hEKYuNmoLpG+E8xHWXu3Uw=='

# Wrapping socket around flask app
socketio = SocketIO(app, cors_allowed_origins="*")

@socketio.on('my_event')
def handleMessage(data):
    print("**Received message: "+str(data)+"**")
    return data

if __name__ == '__main__':
    socketio.run(app, port=6000, debug=True)