# SYNTHETIC AUDIO DATA GENERATION

In this directory, we have script that generate audio data using Google TTS. We will be using our local dashboard to generate the audio data.

# Script to run generate audios
To understand about the script, please run the help command.
```
$ python generate_audio.py --HELP
```

**Example:**
```
$ python generate_audio.py --DOMAIN=kbl_clean_india --FILE=./original_text_data.csv --ROWS=500 --VOICEID=english-female,english-male --ENTERPRISE_ID=karnataka_bank --USER_ID=admin@i2ce.in --PASSWORD=password --HOST=http://192.168.0.147:5000
```


**Note:** If you want convert a directory full of audio data to 16K sample rate using FFMPEG, use the below bash command.
```
$ for f in `ls <MAIN_DIR>/*.wav`; do ffmpeg -i $f -vn -ar 16000 -ac 1 ${f/<MAIN_DIR>/<NEW_DIR>}; done
````

Here, the `<MAIN_DIR>` contain all the original audio data and the a new directory `` should be created before to save all the converted audio data.