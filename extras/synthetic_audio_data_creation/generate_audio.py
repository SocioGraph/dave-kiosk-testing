import requests
import json
from argparse import ArgumentParser, SUPPRESS
import csv, os, time
from pydub import AudioSegment
import random
import string

def save_generated_audio(url, id_):
    print("saving the audio file")
    resp = requests.get(url)
    if resp.status_code < 400:
        ts = "{}{}".format(id_,int(time.time()))
        with open("./clean_audio/{}.wav".format(ts), "wb") as fp:
            fp.write(resp.content)
        WAV_AUDIO_PATH = "./clean_audio" 
        wav_audio_data = AudioSegment.from_wav("{}/{}".format(WAV_AUDIO_PATH, ts +".wav"))
        duration = wav_audio_data.duration_seconds

        print("savved the audio file with id :: ", ts)
        return ts, duration
    else:
        return None, None 

def generate_audio(_utterance_id, _utterance, enterprise_id="dave_restaurant", user_id="ananth+dave_restaurant@i2ce.in", password="D@veQSR21", voice_id="english-male", host="https://test.iamdave.ai"):
    print("Genenrating the audio file for :: ", _utterance)
    url = "{}/get-voice".format(host)

    headers = {
      'Content-Type': 'application/json',
    }


    payload = json.dumps({
      "enterprise_id": enterprise_id,
      "user_id": user_id,
      "password": password,
      "voice_id": random.choice(voice_id.split(",")),
      "_utterance": _utterance
    })

    response = requests.request("POST", url, headers=headers, data=payload)
    if response.status_code < 400:
        return save_generated_audio(response.json()["voice"], _utterance_id)
    else:
        print(response.status_code, response.text)
        return None, None

def _yelid_sheet(file_, rows=100, voice_id="english-male", SKIP=0, enterprise_id="dave_restaurant", user_id="ananth+dave_restaurant@i2ce.in", password="D@vei2ce", host="https://test.iamdave.ai"):
    c=0

    print("Loading the audio file :: ", file_)
    with open(file_, "r") as fp:
        rd = csv.DictReader(fp)
        for i in rd:
            if c > rows:
                break
            c+=1
            if c < SKIP:
                continue
            print("Generating for row :: ", c)
            ob = dict(i)

            try:
                filename = ob["file_name"]
            except:
                filename = "".join([random.choice(string.ascii_lowercase) for i in range(5)])

            new_, duration = generate_audio(filename, ob["utterance"], voice_id=voice_id, enterprise_id=enterprise_id, user_id=user_id, password=password, host=host)
            if not new_:
                print("Not able to generate audio for :: ", ob["utterance"])
                continue
            
            new_ob = ob.copy()
            new_ob["file_name"] = new_
            new_ob["duration"] = duration
            yield new_ob 


if __name__=="__main__":

    
    parser = ArgumentParser(description='Configuration generate the audio data',
                            add_help=False)

    parser.add_argument("--DOMAIN", default=None, help="Doamin name. A new directory will be created to store all synthesised audios.")
    parser.add_argument("--FILE", default=None, help="File that contains the utterances for which synthentic audio need to be created.")
    parser.add_argument("--ROWS", default=10, help="Number of audios to be generated.", type=int)
    parser.add_argument("--VOICEID", default="english-male", help="Voice ids for generated audio. For US-English: use `english-female` and `english-male`. For Indian-English, US `` and ``. Multiple IDs can be mentioned with commas and no spaces.")
    parser.add_argument("--SKIP", default=0, help="Skip these number of rows in the file while generating audio.", type=int)

    parser.add_argument("--HOST", default="https://test.iamdave.ai", help="Dashboard URL. Should be loacal or cloud.")
    parser.add_argument("--ENTERPRISE_ID", default="dave_restaurant", help="Enterprise ID to login and to generate.")
    parser.add_argument("--USER_ID", default="ananth+dave_restaurant@i2ce.in", help="User ID to login.")
    parser.add_argument("--PASSWORD", default="D@vei2ce", help="Password to login.")

    args = parser.parse_args()

    if not args.DOMAIN or not args.FILE:
        print("Arguments DOMAIN AND FILE are required")
    else:
        headers=None
        with open(args.FILE, 'r') as f:
            d_reader = csv.DictReader(f)
            headers = d_reader.fieldnames
            headers = headers + ["file_name","duration"]

        with open("./clean_audio_collection_{}.csv".format(args.DOMAIN), "w" if not args.SKIP else "a") as fp:
            wr = csv.DictWriter(fp, fieldnames=headers)
            if not args.SKIP:
                wr.writeheader()
            for row in _yelid_sheet(args.FILE, args.ROWS, args.VOICEID, args.SKIP, enterprise_id=args.ENTERPRISE_ID, host=args.HOST, user_id=args.USER_ID, password=args.PASSWORD):
                wr.writerow(row)


