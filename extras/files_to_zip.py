"""
Adding all the files from a CSV file to a zip file.
"""
import os
import csv
from zipfile import ZipFile

# Main directory where all wav files in the CSV file are located.
MAIN_DIR = "/home/sunil/dave-ai-projects/dave-kiosk-testing/dave_ai_kiosk_testing/data/audio_data/wav/"

# CSV file that contain all the audio filenmaes.
CSV_AUDIO_DATA = "/home/sunil/dave-ai-projects/dave-kiosk-testing/dave_ai_kiosk_testing/data/audio_data/audio_collection_maruti.csv"

# Output zip filename
ZIP_FILE_NAME = "msil_record.zip"

with ZipFile(ZIP_FILE_NAME, "w") as zip:

    with open(CSV_AUDIO_DATA) as csv_file:
        csv_reader = csv.DictReader(csv_file)
        linecount = 0

        for row in csv_reader:
            zip.write(MAIN_DIR + row["file_name"] + ".wav")
            print(f"Added {MAIN_DIR + row['file_name'] + '.wav'} to the zip file {ZIP_FILE_NAME}")
