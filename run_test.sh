# Activate
source pyenv/bin/activate
pip install -r requirements.txt --quiet

# Running benchmarking test
COMPONENT=$1
URL=$2
DOMAIN_DATA=$3
ENTERPRISE_ID=$4
CONV_ID=$5
RUN_TIME=$6
WAIT=$7
HARDWARE=$8
MULTI_USER=true

# Running the benchmarking test
python -m dave_ai_kiosk_testing --COMPONENT $COMPONENT --URL $URL --DOMAIN $DOMAIN_DATA  --MULTI_USER $MULTI_USER --ENTERPRISE_ID $ENTERPRISE_ID --RUN_TIME $RUN_TIME --WAIT $WAIT --HARDWARE $HARDWARE 1>&2
