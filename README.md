# Dave-Kiosk-Testing
This is a repository for load testing the DaveAI framework. This can test individual components as well as the complete testing framework. 

## Installing requirments
1. Create a virtual environment
```
$ virtualenv .venv
```

2. Activate the virtual environment
```
$ source .venv/bin/activate
```

3. Install all required python packages.
```
(.venv)$ pip install -r requirements.txt
```

4. Update dave_ai_kiosk_testing/config.py` file before running the script. 

Note: Go to *Capture resource utilization of the host system* section to undestand about `HOST_URL` variable.

## Getting started
Once all the required python packages are installed, run the following commands in the project directory to understand the arguments to pass.
```
(.venv)$ python -m dave_ai_kiosk_testing -h
```

The system will show following options. 
```
(.venv) sunil@sunil-daveai:~/DaveAIProjects/dave-kiosk-testing$ python -m dave_ai_kiosk_testing -h
usage: __main__.py [--COMPONENT COMPONENT] [--URL URL [URL ...]] [--CONFIG CONFIG] [-h] [--NUM_USERS NUM_USERS] [--SPAWN_RATE SPAWN_RATE]
                   [--WAIT WAIT] [--RUN_TIME RUN_TIME] [--DOMAIN DOMAIN] [--SSL_VERIFY SSL_VERIFY] [--K_MODEL K_MODEL] [--RECG RECG]
                   [--CHUNKS CHUNKS] [--HARDWARE HARDWARE [HARDWARE ...]]

Configuration to run bechamarking.

required arguments:
  --COMPONENT COMPONENT
                        Mention component to start benchmarking.
  --URL URL [URL ...]   Mention the target URL(s).
  --CONFIG CONFIG       Running the testing framework using a config file.

optional arguments:
  -h, --help            show this help message and exit
  --NUM_USERS NUM_USERS
                        Peak number of concurrent Locust users. Default value: 1
  --SPAWN_RATE SPAWN_RATE
                        Rate to spawn users at (users per second). Default value: 1
  --WAIT WAIT           Wait time between each user load. Default value: 2 seconds]
  --RUN_TIME RUN_TIME   Stop after the specified amount of time (in seconds). Default value: 60
  --DOMAIN DOMAIN       Selecting a domain for data to be loaded for load testing.
  --SSL_VERIFY SSL_VERIFY
                        By default, it will be True. User can change it when it is required.
  --K_MODEL K_MODEL     By default, a general kaldi model is be selected.
  --RECG RECG           By default, kaldi is selected as a recognizer.
  --CHUNKS CHUNKS       The audio data will be sent in chunks if it is true.
  --HARDWARE HARDWARE [HARDWARE ...]
                        By default, it will display it as Intel Hardware. During test, mention simple configuration of the device.
```
Once you select the option, the system will run load testing and catch all required data in a CSV files and stored in `reports` directory. 

## Examples

1. **ASR server bechmarking with default arguments** 
Running `ASR` server with IP address `http://127.0.0.1:5050` with default number of users and spwan rate. The load testing stops after 60 seconds.
```
python -m dave_ai_kiosk_testing --COMPONENT ASR --URL http://127.0.0.1:5050
```

2. **ASR server bechmarking with arguments peak num of users 5 , run time of 120 seconds, and karnataka_bank domain**
Running `ASR` server with IP address `http://127.0.0.1:5050` with number of users is 5 and spwan rate 1. The load testing stops after 120 seconds.
```
python -m dave_ai_kiosk_testing --COMPONENT ASR --URL  http://127.0.0.1:5050 --NUM_USERS 5 --SPAWN_RATE 1 --RUN_TIME 120 --DOMAIN karnataka_bank
```
For ASR component, the following domain data are available (list will be updated soon).
* karnataka_bank
* qsr
* restaurant
* general (default)


3. **NLP server bechmarking with default arguments** 
Running `NLP` server with IP address `http://127.0.0.1:5050` with default number of users and spwan rate.
```
python -m dave_ai_kiosk_testing --COMPONENT NLP --URL http://127.0.0.1:5000
```
Here, domain argumnet can also mentioned using `--DOMAIN` argument. For NLP component, the following domain data are available (list will be updated soon).
* karnataka_bank
* restaurant
* general (default)


5. **ASR to NLP bechmarking with default arguments** 
Running `ASR` server with IP address `http://127.0.0.1:5050` and `NLP` server with with IP address `http://127.0.0.1:5050`. Here, the audio data will be sent to `ASR` server and  `ASR` will send recognized text to `NLP` server. 
```
python -m dave_ai_kiosk_testing --COMPONENT ASR_NLP --URL http://127.0.0.1:5050 http://127.0.0.1:5000
```


The system will generate the output at the end of the load testing. For example (ASR testing): 

```
*********************
** Framework Info: **
*********************
Test Component : ASR
Framework SHA: b6a926dcaec03a85326d383ff53524537cea76b6:2021-11-15T16:44:48+05:30
Component SHA: []
Test Ran Time: 11/15/2021, 17:44:02
Num of Users: 1
Test Run Time (seconds): 300
Domain: restaurant
Recognizer: kaldi
Kaldi Model: vosk-model-en-in-0.4
Hardware: Intel i5-7300U RAM 16 GB HDD Ubuntu 18.04

*********************************
** System Resource Utilization **
*********************************
Average CPU Utilisation Percentage: 20.208000000000002
Average Memory Utilisation Percentage: 18.849333333333337

Max CPU Utilisation Percentage: 34.3
Max Memory Utilisation Percentage: 19.1

************************
** ASR General Report **
************************
Total respose sent: 293
Total respose recieved: 293
Average response time (in seconds): 1.2778590604306894
Word error rate (Weighted Average): 0.298469637673313
Word error rate (Overall): 0.2945417095777549

Load testing completed. The reports can be found in the 'reports/FFPDF' directory.

```

All data are collected in CSV format and combined reports and plots will also be generated. All logs, data, reports, and graphs can be found under the directory mentioned in the final output of the load testing.
```
dave-kiosk-testing/reports/FFPDF/
├── analysis_report.txt
├── asr_general_report.csv
├── basic_statistics.txt
├── CPU_utils_timestamp.png
├── logs.log
├── ram_utils_timestamp.png
├── requests_data_rcvd.csv
├── requests_data_sent.csv
├── request_timestamp.png
├── resource_util_combined_report.csv
├── resource_utils.csv
├── test_exceptions.csv
├── test_failures.csv
├── test_stats.csv
└── test_stats_history.csv
```




## Capture resource utilization of the host system
1. Copy files `flask-requirements.txt` and `resource_util_app.py` from `extras` directory to the host system where container is installed.

2. Create a virtual environment
```
$ virtualenv .venv-flask-app
```

3. Activate the virtual environment
```
$ source .venv-flask-app/bin/activate
```

4. Install all required python packages.
```
(.venv-flask-app)$ pip install -r flask-requirements.txt
```

5. Run the flask application on the host system
```
(.venv-flask-app)$ python resource_util_app.py 
Starting queue monitor.
 * Serving Flask app "resource_util_app" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: on
 * Running on all addresses.
   WARNING: This is a development server. Do not use it in a production deployment.
 * Running on http://192.168.0.147:5005/ (Press CTRL+C to quit)
 * Restarting with stat
Starting queue monitor.
 * Debugger is active!
 * Debugger PIN: 111-619-685
```

6. Note down the host address and update the `HOST_URL` variable in `dave_ai_kiosk_testing/config.py`.

7. Make sure the flask app is running before running `dave_ai_kiosk_testing`.
