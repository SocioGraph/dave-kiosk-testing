commit e36ce116ed0cefcc7aaa622bd1558d71aefccf36
Author: sunil <sunil@iamdave.ai>
Date:   Wed Oct 20 22:38:01 2021 +0530

    CHANGELOG.md created

commit f9b92abbbb7439c1c8587362ffbaaba307ea723e
Author: sunil <sunil@iamdave.ai>
Date:   Wed Oct 20 22:33:06 2021 +0530

    Updated requirements.txt

commit dfeb5aee7f1473f55b82e7920cc39618c571684a
Author: sunil <sunil@iamdave.ai>
Date:   Wed Oct 20 22:32:25 2021 +0530

    Updated .GITIGNORE

commit f1b05d01de91ce6d06e28e5ca5c6f772539fbdfe
Author: sunil <sunil@iamdave.ai>
Date:   Wed Oct 20 22:28:32 2021 +0530

    Updated READEME.md

commit e6312d0cf14fb53c7a343189ba05b7ef53365fba
Author: sunil <sunil@iamdave.ai>
Date:   Wed Oct 20 16:04:26 2021 +0530

    Created load testing for ASR to NLP component
    
    > Creation of load testing for ASR to NLP component (WIP)
    > Modifed the load testing code of ASR component
    > Modified the load testing code for NLP component
    > Few improvisation in the analysis part
    > Domain can be selected before running the benchmarking. Data will be downloaded for testing based on the domain.
    > SSL_VERIFY flag can be selected before running the benchamarking
    > CONFIG flag is updated (WIP). Test can be ran through .yml file.

commit 27b0e3451292aea0459315c57739f4955983054f
Author: sunil <sunil@iamdave.ai>
Date:   Wed Oct 13 15:58:16 2021 +0530

    Updating configuration and domain selection for framework
    
    Development of running test using yaml and selecting domain for testing (Completed)
    Added --SSL-VERIFY argument for the framework
    Made some cleaning and fixed errors in analysis.py
    Added SHA information of framework and components to final statistics

commit 5cefe71e6f150d22627883fe1bc1271fb3aaba09
Author: sunil <sunil@iamdave.ai>
Date:   Tue Oct 12 07:29:35 2021 +0530

    Development of running test using yaml and selecting domain for testing (WIP)

commit 8af74383acbedb865bec01c823606648c951f99b
Author: sunil <sunil@iamdave.ai>
Date:   Thu Oct 7 11:17:56 2021 +0530

    Updated version number

commit 576821ec572e52e2297c055b1f2691fcb92f7612
Author: sunil <sunil@iamdave.ai>
Date:   Wed Oct 6 20:40:06 2021 +0530

    Updating accuracy and report generation for ASR server.
    
    > Modified analysis.py
    > Modified asr_server.py
    > Modified resource_util_app.py
    > Modified run.py
    > Modified utils.py
    > Modified main.py

commit 3df364a0e28031987646bf7432f08d3df79c5659
Author: sunil <sunil@iamdave.ai>
Date:   Wed Oct 6 11:18:10 2021 +0530

    Updated README.md

commit 5aac9e3cf87251093bb702333865da0536d1f1bf
Author: sunil <sunil@iamdave.ai>
Date:   Wed Oct 6 11:15:46 2021 +0530

    Fixed NaN error in analysis.py

commit aed59e24054283bab235147a56e7eea849a8ade7
Author: sunil <sunil@iamdave.ai>
Date:   Tue Oct 5 16:43:54 2021 +0530

    Updated requirement.txt

commit ff44d27153e10421a7bbfd9f86e2091d35fce8ba
Author: sunil <sunil@iamdave.ai>
Date:   Tue Oct 5 15:27:56 2021 +0530

    Code clearning in progress..

commit bb82517f3c285ac349a682e7a4df4fec52384650
Author: sunil <sunil@iamdave.ai>
Date:   Tue Oct 5 09:49:36 2021 +0530

    Included testing NLP server
    
    > Made changes in settings.py and config.py
    > Made changes in analysis.py
    > Made changes in main.py -> Code clearning required
    > Made changes to argument passing under __main__.py
    
    Note: The code contains unwanted conditions and constatnts. I will be cleaning them in the next commit.

commit 93cc3b8a823c89ab09c0da88284d7f4011bdcba5
Author: sunil <sunil@iamdave.ai>
Date:   Tue Sep 28 22:04:04 2021 +0530

    Added few more graphs to statistical analysis

commit 9198e9aa01640120eb995d5e41e45befbee7a6d3
Author: sunil <sunil@iamdave.ai>
Date:   Tue Sep 28 15:25:41 2021 +0530

    Updated analysis.py. Creating some graphs to display the utilisation wrt to timestamp.

commit 762dca671c1435a53f0b9ef090396fe812b8fc6e
Author: sunil <sunil@iamdave.ai>
Date:   Tue Sep 28 13:19:18 2021 +0530

    Used pathlib to store and fetch reports.

commit 9e60771b6a9c42970f30d120fda5b53b1b308186
Author: sunil <sunil@iamdave.ai>
Date:   Tue Sep 28 07:42:32 2021 +0530

    Updated instructions in README.md

commit 6f1937b83cdd17d0c3745e7222cfda03ef164fbd
Author: sunil <sunil@iamdave.ai>
Date:   Tue Sep 28 07:40:34 2021 +0530

    Updated instructions in README.md

commit 9760518828de729600558149743964d78009efac
Author: sunil <sunil@iamdave.ai>
Date:   Mon Sep 27 20:51:37 2021 +0530

    Removed saved to CSV error.

commit d370ff26fa899e30cfe9087ca91bb3f2511c71ed
Author: sunil <sunil@iamdave.ai>
Date:   Mon Sep 27 20:49:10 2021 +0530

    Implemented load testing for ASR component
    
    - argpass is implemented to run script
    - inserted settings.py to store general settings

commit 1a147a88517fe1d72c82116417f3c1b6b22658b8
Author: sunil <sunil@iamdave.ai>
Date:   Tue Sep 21 07:54:56 2021 +0530

    Updated README.md

commit 95bba7fcb091bbb2f715bbfe6f3c7890fe066b91
Author: sunil <sunil@iamdave.ai>
Date:   Mon Sep 20 20:37:17 2021 +0530

    Updated screenshot path

commit cc23ddae77c994a5ee3bf10a1c2c0962f05e9845
Author: sunil <sunil@iamdave.ai>
Date:   Mon Sep 20 20:34:20 2021 +0530

    Updated screenshot path

commit c6e17523aa1665f05199066dc77a07f7caf373ab
Author: sunil <sunil@iamdave.ai>
Date:   Mon Sep 20 20:30:09 2021 +0530

    Initial upload. Added all required files.

commit 759694b51f5b596ddbd3bae4d36a1f27e2fad21b
Author: ananth herokuapp access <ananth@i2ce.in>
Date:   Mon Sep 20 10:28:48 2021 +0000

    Initial commit
